<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use Hoa\Protocol\Node\Node;
use PHPUnit\Framework\TestCase;

class MultiplicationOperatorTest extends TestCase
{

    public function testCompute()
    {
        $operator = new MultiplicationOperator();
        $this->assertEquals(50, $operator->compute(new NodeValue(5), new NodeValue(10)));
        $this->assertEquals(55, $operator->compute(new NodeValue(5.5), new NodeValue(10)));
    }

}