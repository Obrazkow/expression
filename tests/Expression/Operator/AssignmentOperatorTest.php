<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\INodeVariable;
use Expression\Node\NodeValue;
use Expression\Operator\Exception\InvalidTypeException;
use PHPUnit\Framework\TestCase;

class AssignmentOperatorTest extends TestCase
{

//if (!$a instanceof INodeVariable) {
//throw new Exception('First variable must be instanceof INodeVariable');
//}
//
//return $a->assign($b->compute());

    public function testSuccessAssign()
    {
        $variableMock = $this->createMock(INodeVariable::class);
        $variableMock->expects($this->once())->method('assign')->with(3);

        $valueMock = $this->createMock(INode::class);
        $valueMock->method('compute')->willReturn(3);
        $valueMock->expects($this->once())->method('compute');
        $operator = new AssignmentOperator();

        $operator->compute($variableMock, $valueMock);
    }

    public function testInvalidVariable()
    {
        $variableMock = $this->createMock(INode::class);

        $valueMock = $this->createMock(INode::class);
        $operator = new AssignmentOperator();

        $this->expectException(InvalidTypeException::class);
        $operator->compute($variableMock, $valueMock);
    }

}