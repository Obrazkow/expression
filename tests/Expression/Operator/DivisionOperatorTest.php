<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use PHPUnit\Framework\TestCase;

class DivisionOperatorTest extends TestCase
{

    public function testTrue()
    {
        $first = $this->createMock(INode::class);
        $first->method('compute')->willReturn(25);
        $first->expects($this->once())->method('compute');

        $second = $this->createMock(INode::class);
        $second->method('compute')->willReturn(5);
        $second->expects($this->once())->method('compute');

        $operator = new DivisionOperator();
        $this->assertEquals(5, $operator->compute($first, $second));
    }

}