<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use PHPUnit\Framework\TestCase;

class NotEqualityOperatorTest extends TestCase
{

    public function testSuccessCompute()
    {
        $first = $this->createMock(INode::class);
        $first->method('compute')->willReturn(5);
        $first->expects($this->once())->method('compute');

        $second = $this->createMock(INode::class);
        $second->method('compute')->willReturn('5');
        $second->expects($this->once())->method('compute');

        $operator = new NotEqualOperator();
        $this->assertEquals(false, $operator->compute($first, $second));
    }

    public function testSuccessCompute2()
    {
        $first = $this->createMock(INode::class);
        $first->method('compute')->willReturn(5);
        $first->expects($this->once())->method('compute');

        $second = $this->createMock(INode::class);
        $second->method('compute')->willReturn(5.0);
        $second->expects($this->once())->method('compute');

        $operator = new NotEqualOperator();
        $this->assertEquals(false, $operator->compute($first, $second));
    }

    public function testFailureCompute()
    {
        $first = $this->createMock(INode::class);
        $first->method('compute')->willReturn(5);
        $first->expects($this->once())->method('compute');

        $second = $this->createMock(INode::class);
        $second->method('compute')->willReturn(5.5);
        $second->expects($this->once())->method('compute');

        $operator = new NotEqualOperator();
        $this->assertEquals(true, $operator->compute($first, $second));
    }

}