<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use Hoa\Protocol\Node\Node;
use PHPUnit\Framework\TestCase;

class NotOperatorTest extends TestCase
{

    public function testCompute()
    {
        $operator = new NotOperator();
        $this->assertEquals(false, $operator->compute(new NodeValue(true)));
        $this->assertEquals(true, $operator->compute(new NodeValue(false)));
        $this->assertEquals(true, $operator->compute(new NodeValue(0)));
        $this->assertEquals(false, $operator->compute(new NodeValue(1)));
        $this->assertEquals(true, $operator->compute(new NodeValue('0')));
        $this->assertEquals(false, $operator->compute(new NodeValue('1')));
    }

}