<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use PHPUnit\Framework\TestCase;

class MinusOperatorTest extends TestCase
{

    public function testSuccessCompute()
    {
        $operator = new MinusOperator();
        $this->assertEquals(5, $operator->compute(new NodeValue(10), new NodeValue(5)));
        $this->assertEquals(0, $operator->compute(new NodeValue(0), new NodeValue(0)));
    }

}