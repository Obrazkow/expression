<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use PHPUnit\Framework\TestCase;

class AndOperatorTest extends TestCase
{

    public function testTrue()
    {
        $successMock = $this->createMock(INode::class);
        $successMock->method('compute')->willReturn(true);
        $successMock->expects($this->once())->method('compute');

        $successMock2 = $this->createMock(INode::class);
        $successMock2->method('compute')->willReturn(true);
        $successMock2->expects($this->once())->method('compute');

        $operator = new AndOperator();
        $this->assertTrue($operator->compute($successMock, $successMock2));
    }

    public function testFalse1()
    {
        $successMock = $this->createMock(INode::class);
        $successMock->method('compute')->willReturn(true);
        $successMock->expects($this->once())->method('compute');

        $successMock2 = $this->createMock(INode::class);
        $successMock2->method('compute')->willReturn(false);
        $successMock2->expects($this->once())->method('compute');

        $operator = new AndOperator();
        $this->assertFalse($operator->compute($successMock, $successMock2));
    }

    public function testFalse2()
    {
        $successMock = $this->createMock(INode::class);
        $successMock->method('compute')->willReturn(false);
        $successMock->expects($this->once())->method('compute');

        $successMock2 = $this->createMock(INode::class);
        $successMock2->method('compute')->willReturn(true);
        $successMock2->expects($this->never())->method('compute');

        $operator = new AndOperator();
        $this->assertFalse($operator->compute($successMock, $successMock2));
    }

    public function testFalse3()
    {
        $successMock = $this->createMock(INode::class);
        $successMock->method('compute')->willReturn(false);
        $successMock->expects($this->once())->method('compute');

        $successMock2 = $this->createMock(INode::class);
        $successMock2->method('compute')->willReturn(false);
        $successMock2->expects($this->never())->method('compute');

        $operator = new AndOperator();
        $this->assertFalse($operator->compute($successMock, $successMock2));
    }

}