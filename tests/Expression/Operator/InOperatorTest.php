<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use Expression\Structure\IterableStructureAbstract;
use Expression\Structure\ListStructure;
use PHPUnit\Framework\TestCase;

class InOperatorTest extends TestCase
{

    public function testCompute()
    {
        $operator = new InOperator();
        $this->assertEquals(false, $operator->compute(new NodeValue(123), new NodeValue('123')));
        $this->assertEquals(true, $operator->compute(new NodeValue('123'), new NodeValue('123')));
        $this->assertEquals(true, $operator->compute(new NodeValue('123'), new NodeValue('123a')));
        $this->assertEquals(true, $operator->compute(new NodeValue('123'), new NodeValue(new ListStructure(['123', '55']))));
        $this->assertEquals(false, $operator->compute(new NodeValue('123'), new NodeValue(new ListStructure(['1223', '55']))));
    }


}