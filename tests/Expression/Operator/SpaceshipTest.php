<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use PHPUnit\Framework\TestCase;

class SpaceshipTest extends TestCase
{

    public function testCompute()
    {
        $operator = new SpaceshipOperator();
        $this->assertEquals(-1, $operator->compute(new NodeValue(0), new NodeValue(1)));
        $this->assertEquals(0, $operator->compute(new NodeValue(0), new NodeValue(0)));
        $this->assertEquals(1, $operator->compute(new NodeValue(1), new NodeValue(0)));
    }

}