<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use PHPUnit\Framework\TestCase;

class LTOperatorTest extends TestCase
{

    public function testSuccessCompute()
    {
        $operator = new LTOperator();
        $this->assertEquals(false, $operator->compute(new NodeValue(5.1), new NodeValue(5)));
        $this->assertEquals(false, $operator->compute(new NodeValue(5), new NodeValue(5)));
        $this->assertEquals(true, $operator->compute(new NodeValue(4.9), new NodeValue(5)));
    }

}