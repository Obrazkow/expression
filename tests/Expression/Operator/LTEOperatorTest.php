<?php


namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\NodeValue;
use PHPUnit\Framework\TestCase;

class LTEOperatorTest extends TestCase
{

    public function testSuccessCompute()
    {
        $operator = new LTEOperator();
        $this->assertEquals(false, $operator->compute(new NodeValue(5.1), new NodeValue(5)));
        $this->assertEquals(true, $operator->compute(new NodeValue(5), new NodeValue(5)));
        $this->assertEquals(true, $operator->compute(new NodeValue(4.9), new NodeValue(5)));
    }

}