<?php

namespace Expression\Node;

use Expression\Operator\IBinaryOperator;
use Expression\Operator\IUnaryOperator;
use Expression\Scope\Scope;
use Expression\Scope\ScopeRef;
use PHPUnit\Framework\TestCase;

class NodeVarTest extends TestCase
{

    public function testCompute()
    {
        $mock = $this->createMock(ScopeRef::class);
        $mock->expects($this->once())->method('resolveVariable')->with('test');
        $node = new NodeVar('test', $mock);
        $node->compute();
    }

    public function testAsign()
    {
        $mock = $this->createMock(ScopeRef::class);
        $mock->expects($this->once())->method('setVariable')->with('test', 5);
        $node = new NodeVar('test', $mock);
        $node->assign(5);
    }

    public function testGetName()
    {
        $node = new NodeVar('test', $this->createMock(ScopeRef::class));
        $this->assertEquals('test', $node->getName());
    }


}