<?php


namespace Expression\Node;


use Expression\Scope\Exception\UndefinedVariable;
use Expression\Scope\Scope;
use PHPUnit\Framework\TestCase;

class NodeAppTest extends TestCase
{


    public function testReturnNull()
    {
        $nodeBlock = new NodeBlock();
        $nodeBlock->add(new NodeValue(123));
        $node = new NodeApp(Scope::default(), $nodeBlock);
        $this->assertNull($node->compute());
    }

    public function testReturnValue()
    {
        $nodeBlock = new NodeBlock();
        $nodeBlock->add(new NodeReturn(new NodeValue(123)));
        $node = new NodeApp(Scope::default(), $nodeBlock);
        $this->assertEquals(123, $node->compute());
    }

    public function testResetScope()
    {
        $scope = Scope::default();
        $ref1 = $scope->generateRef();
        $ref1->initVariable('test', 1);
        $ref2 = $scope->generateRef();;
        $ref2->initVariable('test', 0);

        $node = new NodeApp($scope, new NodeBlock());
        $node->compute();

        $this->expectException(UndefinedVariable::class);
        $ref1->resolveVariable('test');

        $this->expectException(UndefinedVariable::class);
        $ref2->resolveVariable('test');

    }

}