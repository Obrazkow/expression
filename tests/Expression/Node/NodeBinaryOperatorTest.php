<?php

namespace Expression\Node;

use Expression\Operator\IBinaryOperator;
use PHPUnit\Framework\TestCase;

class NodeBinaryOperatorTest extends TestCase
{

    public function testCompute()
    {
        $arg1 = new NodeValue(1);
        $arg2 = new NodeValue(2);

        $mock = $this->createMock(IBinaryOperator::class);
        $mock->expects($this->once())
            ->method('compute')
            ->with($arg1, $arg2);

        $nodeBinaryOperator = new NodeBinaryOperator($mock, $arg1, $arg2);

        return $nodeBinaryOperator->compute();
    }

}