<?php

namespace Expression\Node;

use Expression\Token;
use PHPUnit\Framework\TestCase;

class NodeNumberTest extends TestCase
{

    public function testCompute()
    {
        $node = new NodeNumber('3');
        $this->assertTrue(3 === $node->compute());

        $node = new NodeNumber('3.5');
        $this->assertTrue(3.5 === $node->compute());

        $node = new NodeNumber('3.5123123');
        $this->assertTrue(3.5123123 === $node->compute());

        $node = new NodeNumber(0.0);
        $this->assertTrue(0.0 === $node->compute());

        $node = new NodeNumber(3.0);
        $this->assertTrue(3.0 === $node->compute());

        $node = new NodeNumber(-1);
        $this->assertTrue(-1 === $node->compute());
    }


}
