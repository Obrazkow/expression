<?php

namespace Expression\Node;

use Expression\Scope\ScopeRef;
use Expression\Token;
use PHPUnit\Framework\TestCase;

class NodeLoopForTest extends TestCase
{

    public function testLoop()
    {
        $initialAction = $this->createMock(INode::class);
        $initialAction->expects($this->once())->method('compute');

        $conditionMock = $this->createMock(INode::class);
        $conditionMock
            ->method('compute')
            ->willReturnCallback(function () {
                static $i = 0;

                return 4 !== $i++;
            });
        $conditionMock->expects($this->exactly(5))->method('compute');

        $iterationMock = $this->createMock(INode::class);
        $iterationMock->expects($this->exactly(4))->method('compute');

        $scope = $this->createMock(ScopeRef::class);
        $scope->expects($this->exactly(4))->method('reset');
        $node = new NodeLoopFor(
            $initialAction,
            $conditionMock,
            $iterationMock,
            $scope,
            new NodeBlock()
        );
        $this->assertNull($node->compute());
    }

    public function testLoopInterruptionContinue()
    {
        $mock = $this->createMock(INode::class);

        $conditionMock = $this->createMock(INode::class);
        $conditionMock
            ->method('compute')
            ->willReturnCallback(function () {
                static $i = 0;

                return 4 !== $i++;
            });
        $conditionMock->expects($this->exactly(5))->method('compute');

        $statementMock = $this->createMock(INode::class);
        $statementMock->expects($this->never())->method('compute');

        $block = new NodeBlock();
        $block->add(new NodeLoopInterruption(new NodeValue(Token::T_LOOP_CONTINUE)));
        $block->add($statementMock);

        $node = new NodeLoopFor($mock, $conditionMock, $mock, $this->createMock(ScopeRef::class), $block);
        $this->assertNull($node->compute());
    }

    public function testLoopInterruptionBreak()
    {
        $mock = $this->createMock(INode::class);

        $conditionMock = $this->createMock(INode::class);
        $conditionMock
            ->method('compute')
            ->willReturnCallback(function () {
                static $i = 0;

                return 4 !== $i++;
            });
        $conditionMock->expects($this->once())->method('compute');

        $statementMock = $this->createMock(INode::class);
        $statementMock->expects($this->never())->method('compute');

        $block = new NodeBlock();
        $block->add(new NodeLoopInterruption(new NodeValue(Token::T_LOOP_BREAK)));
        $block->add($statementMock);

        $node = new NodeLoopFor($mock, $conditionMock, $mock, $this->createMock(ScopeRef::class), $block);
        $this->assertNull($node->compute());

    }

    public function testLoopInterruptionReturn()
    {
        $mock = $this->createMock(INode::class);

        $conditionMock = $this->createMock(INode::class);
        $conditionMock
            ->method('compute')
            ->willReturnCallback(function () {
                static $i = 0;

                return 4 !== $i++;
            });
        $conditionMock->expects($this->once())->method('compute');

        $statementMock = $this->createMock(INode::class);
        $statementMock->expects($this->never())->method('compute');

        $returnNode = new NodeReturn(new NodeValue(5));
        $block = new NodeBlock();
        $block->add($returnNode);
        $block->add($statementMock);

        $node = new NodeLoopFor($mock, $conditionMock, $mock, $this->createMock(ScopeRef::class), $block);
        $this->assertEquals($returnNode, $node->compute());
    }


}
