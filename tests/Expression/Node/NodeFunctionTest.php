<?php

namespace Expression\Node;

use Exception;
use Expression\Scope\Exception\UndefinedVariable;
use Expression\Scope\Exception\VariableAlreadyDefined;
use Expression\Scope\Scope;
use Expression\Scope\ScopeRef;
use PHPUnit\Framework\TestCase;

class NodeFunctionTest extends TestCase
{

    /** @var ScopeRef */
    protected $ref;

    public function setUp()
    {
        parent::setUp();
        $this->ref = Scope::default()->generateRef();
    }

    public function testVariableInitialization()
    {
        $mock = $this->createMock(NodeVarList::class);
        $mock
            ->expects($this->once())
            ->method('compute')
            ->with(1 ,2);

        $node = new NodeFunction(new NodeValue('test'), $mock, new NodeValue(1), $this->ref);
        $node->compute(1, 2);
    }

    public function testWithoutVariableInitialization()
    {
        $mock = $this->createMock(NodeVarList::class);
        $mock
            ->expects($this->once())
            ->method('compute')
            ->with();

        $node = new NodeFunction(new NodeValue('test'), $mock, new NodeValue(1), $this->ref);
        $node->compute();
    }

    public function testWithOneVariableInitialization()
    {
        $mock = $this->createMock(NodeVarList::class);
        $mock
            ->expects($this->once())
            ->method('compute')
            ->with(1);

        $node = new NodeFunction(new NodeValue('test'), $mock, new NodeValue(1), $this->ref);
        $node->compute(1);
    }

    public function testNodeBlockWithReturn()
    {
        $returnMock = $this->createMock(NodeReturn::class);
        $returnMock->method('compute')->willReturn(3);
        $returnMock->expects($this->once())->method('compute');

        $body = $this->createMock(NodeBlock::class);
        $body->method('compute')->willReturn($returnMock);
        $body->expects($this->once())->method('compute');

        $node = new NodeFunction(new NodeValue('test'), new NodeVarList($this->ref), $body, $this->ref);
        $this->assertEquals(3, $node->compute());
    }

    public function testNodeBlockWithoutReturn()
    {
        $body = $this->createMock(NodeBlock::class);
        $body->method('compute')->willReturn(new NodeValue(3));
        $body->expects($this->once())->method('compute');

        $node = new NodeFunction(new NodeValue('test'), new NodeVarList($this->ref), $body, $this->ref);
        $this->assertEquals(null, $node->compute());
    }

    public function testSingleReturnStatement()
    {
        $body = $this->createMock(NodeReturn::class);
        $body->method('compute')->willReturn(3);
        $body->expects($this->once())->method('compute');

        $node = new NodeFunction(new NodeValue('test'), new NodeVarList($this->ref), $body, $this->ref);
        $this->assertEquals(3, $node->compute());
    }

    public function testSingleNonReturnStatement()
    {
        $body = $this->createMock(INode::class);
        $body->method('compute')->willReturn(2);
        $body->expects($this->once())->method('compute');

        $node = new NodeFunction(new NodeValue('test'), new NodeVarList($this->ref), $body, $this->ref);
        $this->assertEquals(null, $node->compute());
    }

    public function testScopeReset()
    {
        $ref = Scope::default()->generateRef();
        $ref->initVariable('test', true);
        $node = new NodeFunction(new NodeValue('test'), new NodeVarList($this->ref), new NodeValue(3), $ref);
        $node->compute();
        $this->expectException(UndefinedVariable::class);
        $ref->resolveVariable('test');
    }

    public function testInvoke()
    {
        $node = new NodeFunction(new NodeValue('test'), new NodeVarList($this->ref), new NodeReturn(new NodeValue(3)), $this->ref);
        $this->assertEquals(3, $node());
    }

}