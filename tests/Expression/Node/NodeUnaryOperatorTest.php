<?php

namespace Expression\Node;

use Expression\Operator\IBinaryOperator;
use Expression\Operator\IUnaryOperator;
use PHPUnit\Framework\TestCase;

class NodeUnaryOperatorTest extends TestCase
{

    public function testCompute()
    {
        $arg1 = new NodeValue(1);

        $mock = $this->createMock(IUnaryOperator::class);
        $mock->expects($this->once())
            ->method('compute')
            ->with($arg1);

        $nodeBinaryOperator = new NodeUnaryOperator($mock, $arg1);

        return $nodeBinaryOperator->compute();
    }

}