<?php

namespace Expression\Node;


use PHPUnit\Framework\TestCase;

class NodeBlockTest extends TestCase
{

    public function testAddArgument()
    {
        $firstArg = new NodeValue(3);
        $secondArg = new NodeValue(1);

        $node = new NodeBlock();
        $node->add($firstArg);
        $node->add($secondArg);

        $reflectionProperty = new \ReflectionProperty(NodeBlock::class, 'nodes');
        $reflectionProperty->setAccessible(true);
        $values = $reflectionProperty->getValue($node);
        $this->assertCount(2, $values);
        $this->assertEquals($firstArg, $values[0]);
        $this->assertEquals($secondArg, $values[1]);

        $node = new NodeBlock();
        $node->add($firstArg);
        $values = $reflectionProperty->getValue($node);
        $this->assertCount(1, $values);
        $this->assertEquals($firstArg, $values[0]);

        $node = new NodeBlock();
        $values = $reflectionProperty->getValue($node);
        $this->assertCount(0, $values);
    }

    public function testComputeReturnInterruption()
    {
        $firstArg = $this->createMock(NodeValue::class);
        $firstArg->expects($this->once())->method('compute');

        $secondArg = $this->createMock(NodeReturn::class);
        $secondArg->expects($this->never())->method('compute');

        $thirdArg = $this->createMock(NodeValue::class);
        $thirdArg->expects($this->never())->method('compute');


        $node = new NodeBlock();
        $node->add($firstArg);
        $node->add($secondArg);
        $node->add($thirdArg);
        $node->compute();
    }

    public function testComputeLateReturnInterruption()
    {
        $return = $this->createMock(NodeReturn::class);
        $return->expects($this->never())->method('compute');

        $firstArg = $this->createMock(INode::class);
        $firstArg->method('compute')->willReturn($return);
        $firstArg->expects($this->once())->method('compute');

        $secondArg = $this->createMock(NodeValue::class);
        $secondArg->expects($this->never())->method('compute');

        $thirdArg = $this->createMock(NodeValue::class);
        $thirdArg->expects($this->never())->method('compute');


        $node = new NodeBlock();
        $node->add($firstArg);
        $node->add($secondArg);
        $node->add($thirdArg);
        $node->compute();
    }

    public function testReturnNull()
    {
        $node = new NodeBlock();
        $this->assertNull($node->compute());
    }

}