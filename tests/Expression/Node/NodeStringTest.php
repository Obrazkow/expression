<?php

namespace Expression\Node;

use PHPUnit\Framework\TestCase;

class NodeStringTest extends TestCase
{

    public function testCompute()
    {
        $node = new NodeString(123);
        $this->assertEquals('123', $node->compute());

        $node = new NodeString('123');
        $this->assertEquals('123', $node->compute());

        $node = new NodeString(123.123);
        $this->assertEquals('123.123', $node->compute());
    }

}
