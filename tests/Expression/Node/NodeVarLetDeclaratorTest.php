<?php

namespace Expression\Node;

use Expression\Operator\IBinaryOperator;
use Expression\Operator\IUnaryOperator;
use Expression\Scope\Scope;
use Expression\Scope\ScopeRef;
use PHPUnit\Framework\TestCase;

class NodeVarLetDeclaratorTest extends TestCase
{

    public function testCompute()
    {
        $mock = $this->createMock(ScopeRef::class);
        $mock->expects($this->once())->method('initVariable')->with('test');
        $node = new NodeVarLetDeclarator(new NodeValue('test'), $mock);
        $node->compute();
    }

}