<?php

namespace Expression\Node;

use Exception;
use Expression\Structure\HashMapStructure;
use Expression\Structure\StructureInterface;
use PHPUnit\Framework\TestCase;
use stdClass;

class NodeObjectTest extends TestCase
{

    public function testGetProperty()
    {
        $object = new HashMapStructure();
        $object->__set('test', 3);

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, new NodeString('test'));
        $this->assertEquals(3, $node->compute());
    }

    public function testGetCallbackMethodFromObject()
    {
        $object = new class () implements StructureInterface {
            public function test()
            {
                return 3;
            }
        };

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, new NodeString('test'));
        $this->assertEquals([$object, 'test'], $node->compute());
    }

    public function testInvalidTypeAtCompute()
    {
        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn(3);
        $nodeVariable->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, new NodeString('test'));
        $this->expectException(Exception::class);
        $node->compute();
    }

    public function testInvalidTypeAtAssign()
    {
        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn(3);
        $nodeVariable->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, new NodeString('test'));
        $this->expectException(Exception::class);
        $node->assign(4);
    }

    public function testSetPropertyValue()
    {
        $object = new HashMapStructure();

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, new NodeString('test'));
        $node->assign(10);
        $this->assertEquals(10, $object->__get('test'));
    }

    public function testSetPropertyString()
    {
        $object = new HashMapStructure();

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $nodeProperty = $this->createMock(NodeVar::class);
        $nodeProperty->method('compute')->willReturn('test');
        $nodeProperty->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, $nodeProperty);
        $node->assign(5);
        $this->assertEquals(5, $object->test);
    }

    public function testGetPropertyString()
    {
        $object = new HashMapStructure();
        $object->__set('test', '5');

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $nodeProperty = $this->createMock(NodeVar::class);
        $nodeProperty->method('compute')->willReturn('test');
        $nodeProperty->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, $nodeProperty);
        $this->assertEquals('5', $node->compute());
    }

    public function testSetPropertyInteger()
    {
        $object = new HashMapStructure();

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $nodeProperty = $this->createMock(NodeVar::class);
        $nodeProperty->method('compute')->willReturn('5');
        $nodeProperty->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, $nodeProperty);
        $node->assign(75);
        $this->assertEquals(75, $object->__get(5));
    }

    public function testGetPropertyInteger()
    {
        $object = new HashMapStructure();
        $object->__set(55, true);

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $nodeProperty = $this->createMock(NodeVar::class);
        $nodeProperty->method('compute')->willReturn(55);
        $nodeProperty->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, $nodeProperty);
        $this->assertEquals(true, $node->compute());
    }

    /**
     * @dataProvider illegalOffsetTypeProvider
     * @param $value
     * @throws Exception
     */
    public function testGetPropertyWithInvalidPropertyType($value)
    {
        $object = new HashMapStructure();

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $nodeProperty = $this->createMock(NodeVar::class);
        $nodeProperty->method('compute')->willReturn($value);
        $nodeProperty->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, $nodeProperty);
        $this->expectException(Exception::class);
        $node->compute();
    }

    /**
     * @dataProvider illegalOffsetTypeProvider
     * @param $value
     * @throws Exception
     */
    public function testSetPropertyWithInvalidPropertyType($value)
    {
        $object = new HashMapStructure();

        $nodeVariable = $this->createMock(NodeVar::class);
        $nodeVariable->method('compute')->willReturn($object);
        $nodeVariable->expects($this->once())->method('compute');

        $nodeProperty = $this->createMock(NodeVar::class);
        $nodeProperty->method('compute')->willReturn($value);
        $nodeProperty->expects($this->once())->method('compute');

        $node = new NodeObject($nodeVariable, $nodeProperty);
        $this->expectException(Exception::class);
        $node->assign(55);
    }

    public function illegalOffsetTypeProvider()
    {
        return [
            'Case1' => [new stdClass()],
            'Case2' => [function() {}],
            'Case3' => [3.5],
            'Case4' => [false],
            'Case5' => [true],
            'Case6' => [null],
            'Case7' => [[]],
        ];
    }

}
