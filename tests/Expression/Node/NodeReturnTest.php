<?php

namespace Expression\Node;

use PHPUnit\Framework\TestCase;

class NodeReturnTest extends TestCase
{

    public function testCompute()
    {
        $mock = $this->createMock(INode::class);
        $mock->method('compute')->willReturn(10);
        $mock->expects($this->once())->method('compute');

        $node = new NodeReturn($mock);
        $this->assertEquals(10, $node->compute());
    }

}
