<?php

namespace Expression\Node;

use PHPUnit\Framework\TestCase;

class NodeFunctionCallerTest extends TestCase
{

    public function testCompute()
    {
        $varMock = $this->createMock(INode::class);
        $varMock
            ->method('compute')
            ->willReturn(function (...$data) {
                $this->assertEquals([1, 2, 3], $data);
            });

        $varMock->expects($this->once())->method('compute');

        $argsMock = $this->createMock(NodeArguments::class);
        $argsMock->method('compute')->willReturn([1, 2, 3]);
        $argsMock->expects($this->once())->method('compute');

        $node = new NodeFunctionCaller($varMock, $argsMock);
        $node->compute();
    }

}
