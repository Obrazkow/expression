<?php

namespace Expression\Node;

use Expression\Node\Exception\InvalidArgumentException;
use Expression\Scope\Scope;
use Expression\Scope\ScopeRef;
use PHPUnit\Framework\TestCase;

class NodeVarListTest extends TestCase
{

    public function testAddArgument()
    {
        $reflectionClass = new \ReflectionProperty(NodeVarList::class, 'variables');
        $reflectionClass->setAccessible(true);

        $ref = Scope::default()->generateRef();
        $node = new NodeVarList($ref);
        $this->assertCount(0, $reflectionClass->getValue($node));
        $var1 = new NodeVar('test1', $ref);
        $node->addArgument($var1);
        $data = $reflectionClass->getValue($node);
        $this->assertEquals($var1, $data[0]);
        $this->assertCount(1, $reflectionClass->getValue($node));
        $var2 = new NodeVar('test2', $ref);
        $node->addArgument($var2);
        $data = $reflectionClass->getValue($node);
        $this->assertEquals($var1, $data[0]);
        $this->assertEquals($var2, $data[1]);
        $this->assertCount(2, $reflectionClass->getValue($node));
    }

    public function testCompute()
    {
        $refMock = $this->createMock(ScopeRef::class);
        $refMock
            ->expects($this->exactly(2))
            ->method('initVariable')
            ->withConsecutive(
                ['test1', 1],
                ['test2', 3]
            );
        $node = new NodeVarList($refMock);

        $varMock1 = $this->createMock(NodeVar::class);
        $varMock1->method('getName')->willReturn('test1');
        $varMock1->expects($this->once())->method('getName');

        $varMock2 = $this->createMock(NodeVar::class);
        $varMock2->method('getName')->willReturn('test2');
        $varMock2->expects($this->once())->method('getName');

        $node->addArgument($varMock1);
        $node->addArgument($varMock2);
        $node->compute(1, 3);
    }

    public function testToFewArguments()
    {
        $refMock = $this->createMock(ScopeRef::class);
        $node = new NodeVarList($refMock);

        $varMock1 = $this->createMock(NodeVar::class);
        $varMock1->expects($this->never())->method('getName');

        $varMock2 = $this->createMock(NodeVar::class);
        $varMock2->expects($this->never())->method('getName');

        $node->addArgument($varMock1);
        $node->addArgument($varMock2);
        $this->expectException(InvalidArgumentException::class);
        $node->compute(1);

        $this->expectException(InvalidArgumentException::class);
        $node->compute();
    }

    public function testExtraArguments()
    {
        $refMock = $this->createMock(ScopeRef::class);
        $refMock
            ->expects($this->exactly(2))
            ->method('initVariable')
            ->withConsecutive(
                ['test1', 1],
                ['test2', 3]
            );
        $node = new NodeVarList($refMock);

        $varMock1 = $this->createMock(NodeVar::class);
        $varMock1->method('getName')->willReturn('test1');
        $varMock1->expects($this->once())->method('getName');

        $varMock2 = $this->createMock(NodeVar::class);
        $varMock2->method('getName')->willReturn('test2');
        $varMock2->expects($this->once())->method('getName');

        $node->addArgument($varMock1);
        $node->addArgument($varMock2);
        $node->compute(1, 3, 5);
    }


}