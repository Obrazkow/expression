<?php

namespace Expression\Node;


use PHPUnit\Framework\TestCase;

class NodeConditionTest extends TestCase
{


    public function awd()
    {
        if ($this->condition->compute()) {
            return $this->body->compute();
        }

        if ($this->chain) {
            return $this->chain->compute();
        }

        return null;
    }

    public function testComputeIfTrue()
    {
        $body = $this->createMock(NodeBlock::class);
        $body->method('compute')->willReturn(2);
        $body->expects($this->once())->method('compute');

        $nodeCondition = new NodeCondition( new NodeValue(true), $body);
        $this->assertEquals(2, $nodeCondition->compute());
    }

    public function testComputeIfFalse()
    {
        $body = $this->createMock(NodeBlock::class);
        $body->expects($this->never())->method('compute');

        $nodeCondition = new NodeCondition( new NodeValue(false), $body);
        $this->assertEquals(null, $nodeCondition->compute());
    }

    public function testComputeChain()
    {
        $firstBody = $this->createMock(NodeBlock::class);
        $firstBody->expects($this->never())->method('compute');

        $body = $this->createMock(NodeBlock::class);
        $body->method('compute')->willReturn(2);
        $body->expects($this->once())->method('compute');

        $chain = new NodeCondition(new NodeValue(true), $body);

        $nodeCondition = new NodeCondition(new NodeValue(false), $firstBody, $chain);
        $this->assertEquals(2, $nodeCondition->compute());
    }

}