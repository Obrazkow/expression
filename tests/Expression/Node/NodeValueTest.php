<?php

namespace Expression\Node;

use Expression\Operator\IBinaryOperator;
use Expression\Operator\IUnaryOperator;
use PHPUnit\Framework\TestCase;

class NodeValueTest extends TestCase
{

    public function testCompute()
    {
        $value = new \stdClass();
        $node = new NodeValue($value);
        $this->assertEquals($value, $node->compute());

        $value = 123;
        $node = new NodeValue($value);
        $this->assertEquals($value, $node->compute());

        $value = '123';
        $node = new NodeValue($value);
        $this->assertEquals($value, $node->compute());

        $value = 123.3;
        $node = new NodeValue($value);
        $this->assertEquals($value, $node->compute());

        $value = true;
        $node = new NodeValue($value);
        $this->assertEquals($value, $node->compute());

        $value = [1,2];
        $node = new NodeValue($value);
        $this->assertEquals($value, $node->compute());

        $value = null;
        $node = new NodeValue($value);
        $this->assertEquals($value, $node->compute());
    }

}