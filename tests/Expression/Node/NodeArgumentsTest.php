<?php

namespace Expression\Node;


use PHPUnit\Framework\TestCase;

class NodeArgumentsTest extends TestCase
{

    public function testAddArgument()
    {
        $node = new NodeArguments();
        $firstArg = new NodeValue(3);
        $secondArg = new NodeValue(1);
        $node->addArgument($firstArg);
        $node->addArgument($secondArg);

        $reflectionProperty = new \ReflectionProperty(NodeArguments::class, 'arguments');
        $reflectionProperty->setAccessible(true);
        $values = $reflectionProperty->getValue($node);
        $this->assertCount(2, $values);
        $this->assertEquals($firstArg, $values[0]);
        $this->assertEquals($secondArg, $values[1]);


        $node = new NodeArguments();
        $values = $reflectionProperty->getValue($node);
        $this->assertCount(0, $values);

        $node = new NodeArguments();
        $node->addArgument($firstArg);
        $values = $reflectionProperty->getValue($node);
        $this->assertCount(1, $values);
        $this->assertEquals($firstArg, $values[0]);
    }

    public function testCompute()
    {
        $firstArg = $this->createMock(NodeValue::class);
        $firstArg->method('compute')->willReturn(3);
        $firstArg->expects($this->once())->method('compute');

        $secondArg = $this->createMock(NodeValue::class);
        $secondArg->method('compute')->willReturn(1);
        $secondArg->expects($this->once())->method('compute');


        $node = new NodeArguments();
        $node->addArgument($firstArg);
        $node->addArgument($secondArg);

        $this->assertEquals([3, 1], $node->compute());

        $firstArg = $this->createMock(NodeValue::class);
        $firstArg->method('compute')->willReturn(3);
        $firstArg->expects($this->once())->method('compute');
        $node = new NodeArguments();
        $node->addArgument($firstArg);
        $this->assertEquals([3], $node->compute());

        $node = new NodeArguments();
        $this->assertEquals([], $node->compute());
    }

}