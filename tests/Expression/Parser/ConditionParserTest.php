<?php


namespace Expression\Parser;


use Comparison\Value;
use Expression\Node\NodeBlock;
use Expression\Node\NodeCondition;
use Expression\Node\NodeNumber;
use Expression\Node\NodeReturn;
use Expression\Node\NodeValue;
use Expression\OperatorsBag;
use Expression\ParsersBag;
use Expression\Scope\Scope;
use Expression\Token;
use Expression\TokenContainer;
use PHPUnit\Framework\TestCase;

class ConditionParserTest extends TestCase
{

    protected $parser;

    public function setUp()
    {
        $this->parser = new ConditionParser(ParsersBag::default(OperatorsBag::default()));
        parent::setUp();
    }

    public function testCan()
    {
        $this->assertTrue($this->parser->can(new Token(Token::T_IF, 'if', 0, 1)));
        $this->assertFalse($this->parser->can(new Token(Token::T_ELSE_IF, 'if', 0, 1)));
        $this->assertFalse($this->parser->can(new Token(Token::T_ELSE, 'if', 0, 1)));
    }

    public function testParseChainElseIf()
    {
        $scope = Scope::default();
        $scope->setConstants('test', true);
        $ref = $scope->generateRef();
        $tokenContainer = new TokenContainer();
        $tokenContainer->add(new Token(Token::T_ELSE_IF, 'else if', 0, 1));
        $tokenContainer->add(new Token(Token::T_BRACKETS_R_OP, '(', 1, 2));
        $tokenContainer->add(new Token(Token::T_NUMBER, 3, 2, 3));
        $tokenContainer->add(new Token(Token::T_BRACKETS_R_CL, ')', 3, 4));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_OP, '{', 4, 5));
        $tokenContainer->add(new Token(Token::T_RETURN, 'return', 5, 6));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 6, 7));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_CL, '}', 7, 8));
        $tokenContainer->add(new Token(Token::EOF, 'eof', 8, 9));

        $method = new \ReflectionMethod(ConditionParser::class, 'parseChain');
        $method->setAccessible(true);

        /** @var NodeCondition $result */
        $result = $method->invoke($this->parser, $ref, $tokenContainer);
        $this->assertInstanceOf(NodeCondition::class, $result);

        $block = new NodeBlock();
        $block->add(new NodeReturn(new NodeNumber(5)));
        $nodeCondition = new NodeCondition(new NodeNumber(3), $block);

        $this->assertEquals($result, $nodeCondition);
    }

    public function testParseChainElseIfWithElse()
    {
        $scope = Scope::default();
        $scope->setConstants('test', true);
        $ref = $scope->generateRef();
        $tokenContainer = new TokenContainer();
        $tokenContainer->add(new Token(Token::T_ELSE_IF, 'else if', 0, 1));
        $tokenContainer->add(new Token(Token::T_BRACKETS_R_OP, '(', 1, 2));
        $tokenContainer->add(new Token(Token::T_NUMBER, 3, 2, 3));
        $tokenContainer->add(new Token(Token::T_BRACKETS_R_CL, ')', 3, 4));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_OP, '{', 4, 5));
        $tokenContainer->add(new Token(Token::T_RETURN, 'return', 5, 6));
        $tokenContainer->add(new Token(Token::T_NUMBER, 1, 6, 7));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_CL, '}', 7, 8));
        $tokenContainer->add(new Token(Token::T_ELSE, 'else', 8, 9));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_OP, '{', 9, 10));
        $tokenContainer->add(new Token(Token::T_RETURN, 'return', 10, 11));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 11, 12));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_CL, '}', 12, 13));
        $tokenContainer->add(new Token(Token::EOF, 'eof', 13, 14));

        $method = new \ReflectionMethod(ConditionParser::class, 'parseChain');
        $method->setAccessible(true);

        /** @var NodeCondition $result */
        $result = $method->invoke($this->parser, $ref, $tokenContainer);
        $this->assertInstanceOf(NodeCondition::class, $result);

        $block = new NodeBlock();
        $block->add(new NodeReturn(new NodeNumber(5)));
        $elseCondition = new NodeCondition(new NodeValue(TRUE), $block);

        $block = new NodeBlock();
        $block->add(new NodeReturn(new NodeNumber(1)));
        $elseIfCondition = new NodeCondition(new NodeNumber(3), $block, $elseCondition);

        $this->assertEquals($result, $elseIfCondition);
    }

    public function testParseChainElse()
    {
        $scope = Scope::default();
        $scope->setConstants('test', true);
        $ref = $scope->generateRef();
        $tokenContainer = new TokenContainer();
        $tokenContainer->add(new Token(Token::T_ELSE, 'else', 0, 1));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_OP, '{', 1, 2));
        $tokenContainer->add(new Token(Token::T_RETURN, 'return', 2, 3));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 3, 4));
        $tokenContainer->add(new Token(Token::T_BRACKETS_C_CL, '}', 4, 5));
        $tokenContainer->add(new Token(Token::EOF, 'eof', 5, 6));

        $method = new \ReflectionMethod(ConditionParser::class, 'parseChain');
        $method->setAccessible(true);
        /** @var NodeCondition $result */
        $result = $method->invoke($this->parser, $ref, $tokenContainer);
        $this->assertInstanceOf(NodeCondition::class, $result);

        $block = new NodeBlock();
        $block->add(new NodeReturn(new NodeNumber(5)));
        $nodeCondition = new NodeCondition(new NodeValue(TRUE), $block);

        $this->assertEquals($result, $nodeCondition);
    }



}