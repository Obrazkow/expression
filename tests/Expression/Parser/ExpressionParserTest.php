<?php


namespace Expression\Parser;


use Expression\Node\NodeBinaryOperator;
use Expression\Node\NodeBlock;
use Expression\Operator\DivisionOperator;
use Expression\Operator\MultiplicationOperator;
use Expression\Operator\PlusOperator;
use Expression\OperatorsBag;
use Expression\ParsersBag;
use Expression\Scope\Scope;
use Expression\Token;
use Expression\TokenContainer;
use PhpParser\Node\Expr\AssignOp\Mul;
use PHPUnit\Framework\TestCase;

class ExpressionParserTest extends TestCase
{

    protected $parser;
    protected $ref;
    protected $parseExpressionMethodReflection;
    protected $nodeBinaryOperatorReflection;
    protected $nodeBinaryArgFirstReflection;
    protected $nodeBinaryArgSecondReflection;

    public function setUp()
    {
        $this->parser = new ExpressionParser(ParsersBag::default(OperatorsBag::default()), OperatorsBag::default());

        $reflectionClass = new \ReflectionClass(NodeBinaryOperator::class);

        $this->nodeBinaryOperatorReflection = $reflectionClass->getProperty('operator');
        $this->nodeBinaryOperatorReflection->setAccessible(true);

        $this->nodeBinaryArgFirstReflection = $reflectionClass->getProperty('argFirst');
        $this->nodeBinaryArgFirstReflection->setAccessible(true);

        $this->nodeBinaryArgSecondReflection = $reflectionClass->getProperty('argSecond');
        $this->nodeBinaryArgSecondReflection->setAccessible(true);

        $this->ref = Scope::default()->generateRef();

        $this->parseExpressionMethodReflection = new \ReflectionMethod(ExpressionParser::class, 'parseExpression');
        $this->parseExpressionMethodReflection->setAccessible(true);

        parent::setUp();
    }

    public function testParseExpression1()
    {
        $tokenContainer = new TokenContainer();
        $tokenContainer->add(new Token(Token::T_NUMBER, 10, 0, 1));
        $tokenContainer->add(new Token(Token::T_OPERATOR_BINARY, '+', 1, 2));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 2, 3));
        $tokenContainer->add(new Token(Token::EOF, 5, 2, 3));

        $body = $this->parseExpressionMethodReflection->invoke($this->parser, $this->ref, $tokenContainer);

        $this->assertInstanceOf(NodeBinaryOperator::class, $body);
        [$operator, $first, $second] = $this->getNodeBinaryOperatorValues($body);

        $this->assertInstanceOf(PlusOperator::class, $operator);
        $this->assertEquals(10, $first->compute());
        $this->assertEquals(5, $second->compute());
    }

    public function testParseExpression2()
    {
        $tokenContainer = new TokenContainer();
        $tokenContainer->add(new Token(Token::T_NUMBER, 10, 0, 1));
        $tokenContainer->add(new Token(Token::T_OPERATOR_BINARY, '+', 1, 2));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 2, 3));
        $tokenContainer->add(new Token(Token::T_OPERATOR_BINARY, '*', 1, 2));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 2, 3));
        $tokenContainer->add(new Token(Token::EOF, 5, 2, 3));

        $body = $this->parseExpressionMethodReflection->invoke($this->parser, $this->ref, $tokenContainer);
        $this->assertInstanceOf(NodeBinaryOperator::class, $body);

        [$operator, $first, $second] = $this->getNodeBinaryOperatorValues($body);
        $this->assertInstanceOf(MultiplicationOperator::class, $operator);
        $this->assertInstanceOf(NodeBinaryOperator::class, $first);
        $this->assertEquals(5, $second->compute());

        [$operator, $first, $second] = $this->getNodeBinaryOperatorValues($first);
        $this->assertInstanceOf(PlusOperator::class, $operator);
        $this->assertEquals(10, $first->compute());
        $this->assertEquals(5, $second->compute());

    }

    public function testParseExpression3()
    {
        $tokenContainer = new TokenContainer();
        $tokenContainer->add(new Token(Token::T_NUMBER, 10, 0, 1));
        $tokenContainer->add(new Token(Token::T_OPERATOR_BINARY, '/', 1, 2));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 2, 3));
        $tokenContainer->add(new Token(Token::T_OPERATOR_BINARY, '*', 1, 2));
        $tokenContainer->add(new Token(Token::T_NUMBER, 5, 2, 3));
        $tokenContainer->add(new Token(Token::EOF, 5, 2, 3));

        $body = $this->parseExpressionMethodReflection->invoke($this->parser, $this->ref, $tokenContainer);
        $this->assertInstanceOf(NodeBinaryOperator::class, $body);

        [$operator, $first, $second] = $this->getNodeBinaryOperatorValues($body);
        $this->assertInstanceOf(MultiplicationOperator::class, $operator);
        $this->assertInstanceOf(NodeBinaryOperator::class, $first);
        $this->assertEquals(5, $second->compute());

        [$operator, $first, $second] = $this->getNodeBinaryOperatorValues($first);
        $this->assertInstanceOf(DivisionOperator::class, $operator);
        $this->assertEquals(10, $first->compute());
        $this->assertEquals(5, $second->compute());
    }

    protected function getNodeBinaryOperatorValues(NodeBinaryOperator $node)
    {
        return [
            $this->nodeBinaryOperatorReflection->getValue($node),
            $this->nodeBinaryArgFirstReflection->getValue($node),
            $this->nodeBinaryArgSecondReflection->getValue($node),
        ];
    }



}