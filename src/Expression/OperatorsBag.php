<?php


namespace Expression;


use Expression\Operator\IBinaryOperator;
use Expression\Operator\InOperator;
use Expression\Operator\IUnaryOperator;
use Expression\Operator\MinusOperator;
use Expression\Operator\NotEqualOperator;
use Expression\Operator\NotIdenticalOperator;
use Expression\Operator\PlusOperator;
use Expression\Operator\AndOperator;
use Expression\Operator\AssignmentOperator;
use Expression\Operator\PostDecrementOperator;
use Expression\Operator\PreDecrementOperator;
use Expression\Operator\DivisionOperator;
use Expression\Operator\EqualOperator;
use Expression\Operator\GTEOperator;
use Expression\Operator\GTOperator;
use Expression\Operator\IdenticalOperator;
use Expression\Operator\PostIncrementOperator;
use Expression\Operator\IOperator;
use Expression\Operator\LTEOperator;
use Expression\Operator\LTOperator;
use Expression\Operator\MultiplicationOperator;
use Expression\Operator\NotOperator;
use Expression\Operator\OrOperator;
use Expression\Operator\PreIncrementOperator;
use Expression\Operator\SpaceshipOperator;

class OperatorsBag
{

    protected $operators = [];

    public static function default(): OperatorsBag
    {
        $bag = new static();
        $bag->add(new NotOperator());
        $bag->add(new AssignmentOperator());
        $bag->add(new MinusOperator());
        $bag->add(new PlusOperator());
        $bag->add(new DivisionOperator());
        $bag->add(new MultiplicationOperator());
        $bag->add(new PreIncrementOperator());
        $bag->add(new PostIncrementOperator());
        $bag->add(new PreDecrementOperator());
        $bag->add(new PostDecrementOperator());
        $bag->add(new NotEqualOperator());
        $bag->add(new EqualOperator());
        $bag->add(new NotIdenticalOperator());
        $bag->add(new IdenticalOperator());
        $bag->add(new GTEOperator());
        $bag->add(new GTOperator());
        $bag->add(new LTEOperator());
        $bag->add(new LTOperator());
        $bag->add(new OrOperator());
        $bag->add(new AndOperator());
        $bag->add(new InOperator());
        $bag->add(new SpaceshipOperator());

        return $bag;
    }

    public function add(IOperator $operator)
    {
        $this->operators[get_class($operator)] = $operator;
    }

    /**
     * @param string $operatorName
     * @return IBinaryOperator|IUnaryOperator
     * @throws \Exception
     */
    public function get(string $operatorName)
    {
        if (!isset($this->operators[$operatorName])) {
            throw new \Exception(sprintf('Operator %s not found in bag', $operatorName));
        }

        return $this->operators[$operatorName];
    }

}