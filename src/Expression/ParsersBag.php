<?php


namespace Expression;


use Expression\Parser\ParserAbstract;
use Expression\Parser\ConditionParser;
use Expression\Parser\ExpressionParser;
use Expression\Parser\LoopParser;
use Expression\Parser\SyntaxParser;

class ParsersBag implements \Iterator
{

    protected $parsers = [];

    public static function default(OperatorsBag $operatorsBag)
    {
        $bag = new static();

        $bag->add(new ExpressionParser($bag, $operatorsBag));
        $bag->add(new ConditionParser($bag));
        $bag->add(new SyntaxParser($bag));
        $bag->add(new LoopParser($bag));

        return $bag;
    }

    public function add(ParserAbstract $parser)
    {
        $this->parsers[get_class($parser)] = $parser;
    }

    /**
     * @param string $parserName
     * @return ParserAbstract
     * @throws \Exception
     */
    public function get(string $parserName): ParserAbstract
    {
        if (!isset($this->parsers[$parserName])) {
            throw new \Exception(sprintf('Parser %s not found in bag', $parserName));
        }

        return $this->parsers[$parserName];
    }

    /**
     * @inheritDoc
     */
    public function current(): ParserAbstract
    {
        return current($this->parsers);
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        next($this->parsers);
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return key($this->parsers);
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return isset($this->parsers[key($this->parsers)]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        reset($this->parsers);
    }

}