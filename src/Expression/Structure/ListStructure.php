<?php

namespace Expression\Structure;


class ListStructure extends IterableStructureAbstract implements StructureInterface
{

    public function push($value)
    {
        $this->elements[] = $value;
    }

    public function addToSet($value)
    {
        if (!in_array($value, $this->elements)) {
            $this->elements[] = $value;
        }
    }

    public function pop()
    {
        return array_pop($this->elements);
    }

    public function shift()
    {
        return array_shift($this->elements);
    }

    public function get(int $position)
    {
        return $this->elements[$position] ?? null;
    }

    public function exists($value): bool
    {
        return in_array($value, $this->elements);
    }

    public function map(callable $f): IterableStructureAbstract
    {
        $elements = [];
        foreach ($this->elements as $k => $v) {
            $elements[] = $f($v, $k);
        }

        return new static($elements);
    }

    public function filter(callable $filter): IterableStructureAbstract
    {
        $elements = [];
        foreach ($this->elements as $k => $v) {
            if ($filter($v, $k)) {
                $elements[] = $v;
            }
        }

        return new static($elements);
    }

}