<?php


namespace Expression\Structure;


use Generator;

class GeneratorStructure implements StructureInterface
{

    protected $generator;

    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    public function map(callable $f): GeneratorStructure
    {
        $generator = (function () use($f) {
            foreach ($this->generator as $k => $v) {
                yield $f($v, $k);
            }
        })();

        return new GeneratorStructure($generator);
    }

    public function filter(callable $filter): GeneratorStructure
    {
        $generator = (function () use($filter) {
            foreach ($this->generator as $k => $v) {
                if ($filter($v, $k)) {
                    yield $v;
                }
            }
        })();

        return new GeneratorStructure($generator);
    }

    public function forEach(callable $f)
    {
        foreach ($this->generator as $k => $v) {
            $f($v, $k);
        }
    }

    public function next()
    {
        $this->generator->next();
    }

    public function current()
    {
        return $this->generator->current();
    }

    public function valid(): bool
    {
        return $this->generator->valid();
    }

    public function key()
    {
        return $this->generator->key();
    }

    public function rewind()
    {
        $this->generator->rewind();
    }


}