<?php


namespace Expression\Structure;


abstract class IterableStructureAbstract
{

    protected $elements = [];

    public function __construct(array $elements = [])
    {
        $this->elements = $elements;
    }

    abstract public function map(callable $f): IterableStructureAbstract;

    abstract public function filter(callable $filter): IterableStructureAbstract;

    public function forEach(callable $f)
    {
        foreach ($this->elements as $k => $v) {
            $f($v, $k);
        }
    }

    public function sort(callable $func)
    {
        $elements = $this->elements;
        usort($elements, $func);

        return new static($elements);
    }

    public function count(): int
    {
        return count($this->elements);
    }

    public function keys(): ListStructure
    {
        return new ListStructure(array_keys($this->elements));
    }

    public function values(): ListStructure
    {
        return new ListStructure(array_values($this->elements));
    }

    public function next()
    {
        next($this->elements);
    }

    public function current()
    {
        return current($this->elements);
    }

    public function valid(): bool
    {
        return array_key_exists($this->key(), $this->elements);
    }

    public function key()
    {
        return key($this->elements);
    }

    public function rewind()
    {
        reset($this->elements);
    }

}