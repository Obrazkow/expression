<?php


namespace Expression\Structure;


class HashMapStructure extends IterableStructureAbstract implements StructureInterface
{

    public function __set($name, $value)
    {
        $this->elements[$name] = $value;
    }

    public function __get($name)
    {
        return $this->elements[$name] ?? null;
    }

    public function map(callable $f): IterableStructureAbstract
    {
        $elements = [];
        foreach ($this->elements as $k => $v) {
            $elements[$k] = $f($v, $k);
        }

        return new static($elements);
    }

    public function filter(callable $filter): IterableStructureAbstract
    {
        $elements = [];
        foreach ($this->elements as $k => $v) {
            if ($filter($v, $k)) {
                $elements[$k] = $v;
            }
        }

        return new static($elements);
    }

}