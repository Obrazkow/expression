<?php

namespace Expression;

use Exception;
use Expression\Operator\AndOperator;
use Expression\Operator\AssignmentOperator;
use Expression\Operator\DivisionOperator;
use Expression\Operator\EqualOperator;
use Expression\Operator\GTEOperator;
use Expression\Operator\GTOperator;
use Expression\Operator\IdenticalOperator;
use Expression\Operator\InOperator;
use Expression\Operator\LTEOperator;
use Expression\Operator\LTOperator;
use Expression\Operator\MinusOperator;
use Expression\Operator\MultiplicationOperator;
use Expression\Operator\NotEqualOperator;
use Expression\Operator\NotIdenticalOperator;
use Expression\Operator\NotOperator;
use Expression\Operator\OrOperator;
use Expression\Operator\PlusOperator;
use Expression\Operator\PreDecrementOperator;
use Expression\Operator\PreIncrementOperator;
use Expression\Operator\SpaceshipOperator;

class LexerSyntax
{

    protected const SYNTAX = [
        'let' => Token::T_VAR_DECLARATOR,
        'return' => Token::T_RETURN,
        'fn' => Token::T_FUNC,
        'if' => Token::T_IF,
        'else if' => Token::T_ELSE_IF,
        'else' => Token::T_ELSE,
        'for' => Token::T_LOOP_FOR,
        'continue' => Token::T_LOOP_CONTINUE,
        'break' => Token::T_LOOP_BREAK,
    ];

    protected const SIGNS = [
        ';' => Token::T_SEMICOLON,
        ',' => Token::T_COMMA,
        '=>' => Token::T_FUNC_ARROW,
    ];

    protected const OPENED_BRACKETS = [
        '{' => Token::T_BRACKETS_C_OP,
        '[' => Token::T_BRACKETS_S_OP,
        '(' => Token::T_BRACKETS_R_OP,
    ];

    protected const CLOSED_BRACKETS = [
        '}' => Token::T_BRACKETS_C_CL,
        ']' => Token::T_BRACKETS_S_CL,
        ')' => Token::T_BRACKETS_R_CL,
    ];

    protected const NAMED_REGEX = [
        'dot' => ['\.', Token::T_DOT],
        'var' => ['[a-zA-Z_][a-zA-Z0-9_]*', Token::T_VAR],
        'number' => ['[0-9]+(?:\.[0-9]+)?', Token::T_NUMBER],
        'text' => ['"([^"]*)"|\'([^\']*)\'', Token::T_TEXT],
    ];

    protected $brackets = [];
    protected $operatorsRegistry = [];

    public static function getDefault(): LexerSyntax
    {
        $lexer = new static();
        $lexer->registerOperator(PlusOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(MinusOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(DivisionOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(MultiplicationOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(NotIdenticalOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(IdenticalOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(NotEqualOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(EqualOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(AssignmentOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(GTOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(GTEOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(LTOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(LTEOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(AndOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(OrOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(SpaceshipOperator::SIGN, Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(' ' . InOperator::SIGN . ' ', Token::T_OPERATOR_BINARY);
        $lexer->registerOperator(NotOperator::SIGN, Token::T_OPERATOR_UNARY);
        $lexer->registerOperator(PreIncrementOperator::SIGN, Token::T_OPERATOR_UNARY);
        $lexer->registerOperator(PreDecrementOperator::SIGN, Token::T_OPERATOR_UNARY);

        return $lexer;
    }

    public function registerOperator(string $code, int $operatorType)
    {
        $this->operatorsRegistry[$code] = $operatorType;
    }

    /**
     * @param string $input
     * @return TokenContainer
     * @throws Exception
     */
    public function tokenize(string $input): TokenContainer
    {
        $input = str_replace(["\n", "\r", "\t", "\v", "\f"], '', $input);
        $length = strlen($input);

        $tokenContainer = new TokenContainer();
        $cursor = 0;
        $pattern = $this->getPattern();
        $token = null;
        while ($cursor < $length) {
            if ($input[$cursor] == ' ') {
                $cursor++;
                continue;
            }

            if (isset(static::OPENED_BRACKETS[$input[$cursor]])) {
                $this->brackets[] = $input[$cursor];
                $token = new Token(static::OPENED_BRACKETS[$input[$cursor]], $input[$cursor], $cursor, $cursor + 1);
            } else if (isset(static::CLOSED_BRACKETS[$input[$cursor]])) {
                if (array_pop($this->brackets) !== strtr($input[$cursor], ')}]', '({[')) {
                    throw new \Exception('Invalid closed bracket');
                };

                $token = new Token(static::CLOSED_BRACKETS[$input[$cursor]], $input[$cursor], $cursor, $cursor + 1);
            } else if (preg_match($pattern, $input, $matches, 0, $cursor)) {
                $t = $matches[0];
                $end = $cursor + strlen($t);
                if (isset(static::SYNTAX[$t])) {
                    $token = new Token(static::SYNTAX[$t], $t, $cursor, $end);
                } else if (isset(static::SIGNS[$t])) {
                    $token = new Token(static::SIGNS[$t], $t, $cursor, $end);
                } else if (isset($this->operatorsRegistry[$t])) {
                    $token = new Token($this->operatorsRegistry[$t], $t, $cursor, $end);
                } else {
                    foreach (static::NAMED_REGEX as $group => $item) {
                        if (isset($matches[$group]) && strlen($matches[$group])) {
                            if ($group === 'text') {
                                if ($t[0] == '"') {
                                    $t = str_replace('"', '', $t);
                                } elseif ($t[0] == '\'') {
                                    $t = str_replace('\'', '', $t);
                                }
                            }

                            $token = new Token($item[1], $t, $cursor, $end);
                        }
                    }
                }
            }

            if (!$token) {
                throw new \Exception('Undefined token');
            }

            //skip semicolon from parsing application
            if (!$token->isType(Token::T_SEMICOLON)) {
                $tokenContainer->add($token);
            }

            $cursor = $token->getEndCursor();
            $token = null;
        }

        $tokenContainer->add(new Token(Token::EOF, null, $cursor, $cursor));

        return $tokenContainer;
    }

    protected function getPattern(): string
    {
        $_operators = array_keys($this->operatorsRegistry);
        array_multisort(array_map('strlen', $_operators), SORT_DESC, $_operators);

        $operators = [];
        foreach ($_operators as $key) {
            $operators[] = preg_quote($key, '/');
        }

        $namedRegex = [];
        foreach (static::NAMED_REGEX as $group => $item) {
            $namedRegex[] = sprintf('(?<%s>%s)', $group, $item[0]);
        }

        // priority
        return sprintf(
            '/((?:%s)\b)|(?:%s)|(?:%s)/A',
            implode('|', array_keys(static::SYNTAX)),
            implode('|', array_merge(array_keys(static::SIGNS), $operators)),
            implode('|', $namedRegex)
        );
    }

}