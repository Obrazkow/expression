<?php


namespace Expression;


use Expression\Node\INode;
use Expression\Node\NodeReturn;

class Evaluator
{

    /**
     * @param INode $node
     * @return mixed
     */
    public function build(INode $node)
    {
        return $node->compute();
    }

}