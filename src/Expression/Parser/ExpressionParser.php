<?php

namespace Expression\Parser;


use Exception;
use Expression\Node\INode;
use Expression\Node\NodeArguments;
use Expression\Node\NodeBlock;
use Expression\Node\NodeFunction;
use Expression\Node\NodeFunctionCaller;
use Expression\Node\NodeHashMapStructure;
use Expression\Node\NodeListStructure;
use Expression\Node\NodeNumber;
use Expression\Node\NodeObject;
use Expression\Node\NodeReturn;
use Expression\Node\NodeString;
use Expression\Node\NodeUnaryOperator;
use Expression\Node\NodeValue;
use Expression\Node\NodeBinaryOperator;
use Expression\Node\NodeVar;
use Expression\Node\NodeVarLetDeclarator;
use Expression\Node\NodeVarList;
use Expression\Operator\AndOperator;
use Expression\Operator\AssignmentOperator;
use Expression\Operator\DivisionOperator;
use Expression\Operator\EqualOperator;
use Expression\Operator\GTEOperator;
use Expression\Operator\GTOperator;
use Expression\Operator\IBinaryOperator;
use Expression\Operator\IdenticalOperator;
use Expression\Operator\InOperator;
use Expression\Operator\IOperator;
use Expression\Operator\IUnaryOperator;
use Expression\Operator\LTEOperator;
use Expression\Operator\LTOperator;
use Expression\Operator\MinusOperator;
use Expression\Operator\MultiplicationOperator;
use Expression\Operator\NotEqualOperator;
use Expression\Operator\NotIdenticalOperator;
use Expression\Operator\NotOperator;
use Expression\Operator\Operator;
use Expression\Operator\OperatorPrecedence;
use Expression\Operator\OrOperator;
use Expression\Operator\PlusOperator;
use Expression\Operator\PostDecrementOperator;
use Expression\Operator\PostIncrementOperator;
use Expression\Operator\PreDecrementOperator;
use Expression\Operator\PreIncrementOperator;
use Expression\Operator\SpaceshipOperator;
use Expression\OperatorsBag;
use Expression\Parser\Exception\UndefinedToken;
use Expression\Parser\Exception\UnexpectedToken;
use Expression\ParsersBag;
use Expression\Scope\ScopeRef;
use Expression\Structure\HashMapStructure;
use Expression\Structure\ListStructure;
use Expression\Token;
use Expression\TokenContainer;

class ExpressionParser extends ParserAbstract
{

    protected const ACCEPT_TOKEN = [
        Token::T_OPERATOR_UNARY,
        Token::T_NUMBER,
        Token::T_TEXT,
        Token::T_VAR,
        Token::T_BRACKETS_R_OP,
        Token::T_BRACKETS_C_OP,
        Token::T_BRACKETS_S_OP,
        Token::T_FUNC
    ];

    protected $operatorsBag;

    public function __construct(ParsersBag $parsersBag, OperatorsBag $operatorsBag)
    {
        parent::__construct($parsersBag);
        $this->operatorsBag = $operatorsBag;
    }

    public function parse(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        return $this->parseExpression($scopeRef, $tokenContainer);
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @param int $precedence
     * @return INode
     * @throws Exception
     */
    protected function parseExpression(ScopeRef $scopeRef, TokenContainer $tokenContainer, $precedence = 0): INode
    {
        $expr = $this->getNode($scopeRef, $tokenContainer);

        $token = $tokenContainer->current();
        while ($token->isType(Token::T_OPERATOR_BINARY)
            && $this->getPrecedenceOfOperator($this->getBinaryOperator($token)) > $precedence
        ) {
            $operator = $this->getBinaryOperator($tokenContainer->shift());
            $curPrecedence = $this->getPrecedenceOfOperator($operator) + (int) $operator->isLeftAssoc();

            $expr = new NodeBinaryOperator($operator, $expr, $this->parseExpression($scopeRef, $tokenContainer, $curPrecedence));

            $token = $tokenContainer->current();
        }

        return $expr;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return INode
     * @throws Exception
     */
    protected function getNode(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        $token = $tokenContainer->current();

        switch ($token->getType()) {
            case Token::T_OPERATOR_UNARY:
                return new NodeUnaryOperator(
                    $this->getUnaryOperator($tokenContainer->shift(), true),
                    $this->getNode($scopeRef, $tokenContainer)
                );
            case Token::T_NUMBER:
                return $this->parsePostfixNode($scopeRef, $tokenContainer, new NodeNumber($tokenContainer->shift()->getValue()));
            case Token::T_TEXT:
                return $this->parsePostfixNode($scopeRef, $tokenContainer, new NodeString($tokenContainer->shift()->getValue()));
            case Token::T_VAR:
                return $this->parsePostfixNode($scopeRef, $tokenContainer, $this->parseVariableExpression($scopeRef, $tokenContainer));
            case Token::T_BRACKETS_R_OP:
                return $this->parseRoundBrackets($scopeRef, $tokenContainer);
            case Token::T_BRACKETS_C_OP:
                return $this->parsePostfixNode($scopeRef, $tokenContainer, $this->parseCurlyBracket($tokenContainer));
            case Token::T_BRACKETS_S_OP:
                return $this->parsePostfixNode($scopeRef, $tokenContainer, $this->parseSquareBracket($tokenContainer));
            case Token::T_FUNC:
                return $this->parseFunction($scopeRef, $tokenContainer);
        }

        throw new UndefinedToken($token);
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @param INode $node
     * @return INode
     * @throws Exception
     */
    protected function parsePostfixNode(ScopeRef $scopeRef, TokenContainer $tokenContainer, INode $node): INode
    {
        while (true) {
            $found = false;

            while ($tokenContainer->current()->isType(Token::T_DOT)) {
                $node = $this->parsePostfixDot($tokenContainer, $node);
                $found = true;
            }

            while ($tokenContainer->current()->isType(Token::T_BRACKETS_S_OP)) {
                $node = $this->parsePostfixSquareBrackets($scopeRef, $tokenContainer, $node);
                $found = true;
            }

            while ($tokenContainer->current()->isType(Token::T_BRACKETS_R_OP)) {
                $node = $this->parsePostfixRoundBrackets($scopeRef, $tokenContainer, $node);
                $found = true;
            }

            if (!$found) break;
        }

        if ($tokenContainer->current()->isType(Token::T_OPERATOR_UNARY)) {
            $node = new NodeUnaryOperator($this->getUnaryOperator($tokenContainer->shift(), false), $node);
        }

        return $node;
    }

    /**
     * @param TokenContainer $tokenContainer
     * @param INode $node
     * @return NodeObject
     * @throws Exception
     */
    protected function parsePostfixDot(TokenContainer $tokenContainer, INode $node): NodeObject
    {
        $tokenContainer->next();
        $propertyName = $tokenContainer->shift();
        if (!$propertyName->isType(Token::T_VAR)) {
            throw new Exception(sprintf('Invalid property name %s', $propertyName->getValue()));
        }

        return new NodeObject($node, new NodeString($propertyName->getValue()));
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @param INode $node
     * @return NodeObject
     * @throws UnexpectedToken
     * @throws Exception
     */
    protected function parsePostfixSquareBrackets(ScopeRef $scopeRef, TokenContainer $tokenContainer, INode $node): NodeObject
    {
        $tokenContainer->next();
        $token = $tokenContainer->current();
        if (!$this->can($token)) {
            throw new UnexpectedToken($token);
        }

        $node = new NodeObject($node, $this->parseExpression($scopeRef, $tokenContainer));
        $this->assertType($tokenContainer->shift(), Token::T_BRACKETS_S_CL);

        return $node;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @param INode $node
     * @return NodeFunctionCaller
     * @throws Exception
     */
    protected function parsePostfixRoundBrackets(ScopeRef $scopeRef, TokenContainer $tokenContainer, INode $node): NodeFunctionCaller
    {
        $tokenContainer->next();

        $arguments = new NodeArguments();
        while (!$tokenContainer->current()->isType(Token::T_BRACKETS_R_CL)) {
            $arguments->addArgument($this->parse($scopeRef, $tokenContainer));
            if ($tokenContainer->current()->isType(Token::T_COMMA)) {
                $tokenContainer->next();
            }
        }
        $tokenContainer->next();

        return new NodeFunctionCaller($node, $arguments);
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return INode
     * @throws Exception
     */
    protected function parseRoundBrackets(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        if ($this->isArrowFunction($tokenContainer)) {
            return $this->parseArrowFunction($scopeRef, $tokenContainer);
        }

        $tokenContainer->next();
        $expr = $this->parseExpression($scopeRef, $tokenContainer);
        $this->assertType($tokenContainer->shift(), Token::T_BRACKETS_R_CL);

        return $this->parsePostfixNode($scopeRef, $tokenContainer, $expr);
    }

    /**
     * @param TokenContainer $tokenContainer
     * @return NodeHashMapStructure
     * @throws Exception
     */
    protected function parseCurlyBracket(TokenContainer $tokenContainer): NodeHashMapStructure
    {
        $tokenContainer->next();
        $this->assertType($tokenContainer->shift(), Token::T_BRACKETS_C_CL);

        return new NodeHashMapStructure();
    }

    /**
     * @param TokenContainer $tokenContainer
     * @return NodeListStructure
     * @throws Exception
     */
    protected function parseSquareBracket(TokenContainer $tokenContainer): NodeListStructure
    {
        $tokenContainer->next();
        $this->assertType($tokenContainer->shift(), Token::T_BRACKETS_S_CL);

        return new NodeListStructure();
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeFunctionCaller|NodeVar
     */
    protected function parseVariableExpression(ScopeRef $scopeRef, TokenContainer $tokenContainer)
    {
        return new NodeVar($tokenContainer->shift()->getValue(), $scopeRef);
    }

    protected function isArrowFunction(TokenContainer $tokenContainer)
    {
        foreach ($tokenContainer->lookAhead(1) as $token) {
            if ($token->isType(Token::T_FUNC_ARROW)) {
                return true;
            }

            if (
                !$token->isType(Token::T_VAR)
                && !$token->isType(Token::T_COMMA)
                && !$token->isType(Token::T_BRACKETS_R_CL)
            ) {
                return false;
            }
        }

        return false;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeValue
     * @throws Exception
     */
    protected function parseArrowFunction(ScopeRef $scopeRef, TokenContainer $tokenContainer): NodeValue
    {
        $functionScopeRef = $scopeRef->getScope()->generateRefChain($scopeRef);
        $functionVariables = $this->parseFunctionVariables($functionScopeRef, $tokenContainer);

        // token (=>)
        $tokenContainer->next();

        return new NodeValue(
            new NodeFunction(
                new NodeValue('anonymous'),
                $functionVariables,
                $this->parseArrowFunctionBlock($functionScopeRef, $tokenContainer),
                $functionScopeRef
            )
        );
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeBlock
     * @throws Exception
     */
    protected function parseFunction(ScopeRef $scopeRef, TokenContainer $tokenContainer)
    {
        $tokenContainer->next();

        $functionScopeRef = $scopeRef->getScope()->generateRef();
        $name = $this->parseName($tokenContainer);
        $functionVariables = $this->parseFunctionVariables($functionScopeRef, $tokenContainer);
        $body = $this->parseFunctionBlock($functionScopeRef, $tokenContainer);

        $block = new NodeBlock();
        $block->add(new NodeVarLetDeclarator($name, $scopeRef));
        $block->add(
            new NodeBinaryOperator(
                new AssignmentOperator(),
                new NodeVar($name->compute(), $scopeRef),
                new NodeValue(
                    new NodeFunction($name, $functionVariables, $body, $functionScopeRef)
                )
            )
        );

        return $block;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return INode
     * @throws Exception
     */
    protected function parseArrowFunctionBlock(ScopeRef $scopeRef, TokenContainer $tokenContainer)
    {
        $node = $this->parseFunctionBlock($scopeRef, $tokenContainer);
        if (!$node instanceof NodeReturn && !$node instanceof NodeBlock) {
            return new NodeReturn($node);
        }

        return $node;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return INode
     * @throws Exception
     */
    protected function parseFunctionBlock(ScopeRef $scopeRef, TokenContainer $tokenContainer)
    {
        if (!$tokenContainer->current()->isType(Token::T_BRACKETS_C_OP)) {
            return $this->parseBlockStatement($scopeRef, $tokenContainer);
        }

        return parent::parseBlock($scopeRef, $tokenContainer);
    }

    /**
     * @param TokenContainer $tokenContainer
     * @return NodeValue
     * @throws Exception
     */
    protected function parseName(TokenContainer $tokenContainer): NodeValue
    {
        $token = $tokenContainer->shift();

        $this->assertType($token, Token::T_VAR);

        return new NodeValue($token->getValue());
    }

    /**
     * @param TokenContainer $tokenContainer
     * @param ScopeRef $scopeRef
     * @return NodeVarList
     * @throws Exception
     */
    protected function parseFunctionVariables(ScopeRef $scopeRef, TokenContainer $tokenContainer): NodeVarList
    {
        $this->assertAndSkip($tokenContainer, Token::T_BRACKETS_R_OP);

        $nodeVariables = new NodeVarList($scopeRef);
        while(!$tokenContainer->current()->isType(Token::T_BRACKETS_R_CL)) {
            /** @var NodeVar $variable */
            $variable = $this->parsersBag->get(ExpressionParser::class)->parse($scopeRef, $tokenContainer);
            $nodeVariables->addArgument($variable);
            if ($tokenContainer->current()->isType(Token::T_COMMA)) {
                $tokenContainer->next();
            }
        }
        $tokenContainer->next();

        return $nodeVariables;
    }

    /**
     * @param Token $token
     * @param bool $isPrefix
     * @return IUnaryOperator
     * @throws Exception|UndefinedToken
     */
    protected function getUnaryOperator(Token $token, bool $isPrefix = true): IUnaryOperator
    {
        switch ($token->getValue()) {
            case NotOperator::SIGN:
                return $this->operatorsBag->get(NotOperator::class);
            case PreIncrementOperator::SIGN:
                return $this->operatorsBag->get($isPrefix ? PreIncrementOperator::class : PostIncrementOperator::class);
            case PreDecrementOperator::SIGN:
                return $this->operatorsBag->get($isPrefix ? PreDecrementOperator::class : PostDecrementOperator::class);
            default:
                throw new UndefinedToken($token);
        }
    }

    protected function getPrecedenceOfOperator(IOperator $operator): int
    {
        return OperatorPrecedence::PRECEDENCE[get_class($operator)];
    }

    /**
     * @param Token $token
     * @return IBinaryOperator
     * @throws Exception|UndefinedToken
     */
    protected function getBinaryOperator(Token $token): IBinaryOperator
    {
        switch ($token->getValue()) {
            case PlusOperator::SIGN:
                return $this->operatorsBag->get(PlusOperator::class);
            case MinusOperator::SIGN:
                return $this->operatorsBag->get(MinusOperator::class);
            case DivisionOperator::SIGN:
                return $this->operatorsBag->get(DivisionOperator::class);
            case MultiplicationOperator::SIGN:
                return $this->operatorsBag->get(MultiplicationOperator::class);
            case NotIdenticalOperator::SIGN:
                return $this->operatorsBag->get(NotIdenticalOperator::class);
            case IdenticalOperator::SIGN:
                return $this->operatorsBag->get(IdenticalOperator::class);
            case NotEqualOperator::SIGN:
                return $this->operatorsBag->get(NotEqualOperator::class);
            case EqualOperator::SIGN:
                return $this->operatorsBag->get(EqualOperator::class);
            case AssignmentOperator::SIGN:
                return $this->operatorsBag->get(AssignmentOperator::class);
            case GTOperator::SIGN:
                return $this->operatorsBag->get(GTOperator::class);
            case GTEOperator::SIGN:
                return $this->operatorsBag->get(GTEOperator::class);
            case LTOperator::SIGN:
                return $this->operatorsBag->get(LTOperator::class);
            case LTEOperator::SIGN:
                return $this->operatorsBag->get(LTEOperator::class);
            case AndOperator::SIGN:
                return $this->operatorsBag->get(AndOperator::class);
            case OrOperator::SIGN:
                return $this->operatorsBag->get(OrOperator::class);
            case InOperator::SIGN:
                return $this->operatorsBag->get(InOperator::class);
            case SpaceshipOperator::SIGN:
                return $this->operatorsBag->get(SpaceshipOperator::class);
            default:
                throw new UndefinedToken($token);
        }
    }

}