<?php

namespace Expression\Parser;


use Exception;
use Expression\Node\INode;
use Expression\Node\NodeBlock;
use Expression\Node\NodeLoopFor;
use Expression\Node\NodeLoopInterruption;
use Expression\Node\NodeValue;
use Expression\Parser\Exception\UndefinedToken;
use Expression\ParsersBag;
use Expression\Scope\ScopeRef;
use Expression\Token;
use Expression\TokenContainer;

class LoopParser extends ParserAbstract
{

    protected const ACCEPT_TOKEN = [
        Token::T_LOOP_FOR
    ];

    protected $activeLoopParser = 0;
    protected $continueInterruption;
    protected $breakInterruption;

    public function __construct(ParsersBag $parsersBag)
    {
        parent::__construct($parsersBag);

        $this->continueInterruption = new NodeLoopInterruption(new NodeValue(Token::T_LOOP_CONTINUE));
        $this->breakInterruption = new NodeLoopInterruption(new NodeValue(Token::T_LOOP_BREAK));
    }

    public function can(Token $token): bool
    {
        return $this->activeLoopParser > 0 || parent::can($token);
    }

    public function parse(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        $token = $tokenContainer->current();

        switch ($token->getType()) {
            case Token::T_LOOP_FOR:
                return $this->parseLoopFor($scopeRef, $tokenContainer);
            case Token::T_LOOP_CONTINUE:
                $tokenContainer->next();
                return $this->continueInterruption;
            case Token::T_LOOP_BREAK:
                $tokenContainer->next();
                return $this->breakInterruption;
        }

        throw new UndefinedToken($token);
    }


    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeLoopFor
     * @throws Exception
     */
    protected function parseLoopFor(ScopeRef $scopeRef, TokenContainer $tokenContainer)
    {
        $this->assertAndSkip($tokenContainer, Token::T_LOOP_FOR);

        [$initiation, $condition, $iteration] = $this->parseForStatements($scopeRef, $tokenContainer);

        $scope = $scopeRef->getScope()->generateRefChain($scopeRef);
        return new NodeLoopFor(
            $initiation,
            $condition,
            $iteration,
            $scope,
            $this->parseBlock(
                $scope,
                $tokenContainer
            )
        );
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return array
     * @throws Exception
     */
    protected function parseForStatements(ScopeRef $scopeRef, TokenContainer $tokenContainer): array
    {
        $this->assertAndSkip($tokenContainer, Token::T_BRACKETS_R_OP);

        $initialAction = $this->parseBlockStatement($scopeRef, $tokenContainer);
        $condition = $this->parseBlockStatement($scopeRef, $tokenContainer);
        $iterationAction = $this->parseBlockStatement($scopeRef, $tokenContainer);

        $this->assertAndSkip($tokenContainer, Token::T_BRACKETS_R_CL);

        return [$initialAction, $condition, $iterationAction];
    }

    protected function parseBlock(ScopeRef $scopeRef, TokenContainer $tokenContainer): NodeBlock
    {
        $this->activeLoopParser++;
        $block = parent::parseBlock($scopeRef, $tokenContainer);
        $this->activeLoopParser--;

        return $block;
    }

}