<?php


namespace Expression\Parser\Exception;


use Exception;
use Expression\Token;

class UndefinedToken extends Exception
{

    public function __construct(Token $token)
    {
        parent::__construct('Undefined token ' . $token->getType());
    }

}