<?php

namespace Expression\Parser;


use Exception;
use Expression\Node\INode;
use Expression\Node\NodeBlock;
use Expression\Parser\Exception\UndefinedToken;
use Expression\ParsersBag;
use Expression\Scope\ScopeRef;
use Expression\Token;
use Expression\TokenContainer;

abstract class ParserAbstract
{

    protected const ACCEPT_TOKEN = [];

    protected $parsersBag;

    public function __construct(ParsersBag $parsersBag)
    {
        $this->parsersBag = $parsersBag;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @throws Exception
     * @return INode
     */
    abstract public function parse(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode;

    public function can(Token $token): bool
    {
        return in_array($token->getType(), static::ACCEPT_TOKEN);
    }

    /**
     * @param Token $token
     * @param int $type
     * @throws Exception
     */
    protected function assertType(Token $token, int $type)
    {
        if (!$token->isType($type)) {
            throw new \Exception(sprintf('Expected %s got %s', $type, $token->getType()));
        }
    }

    /**
     * @param TokenContainer $tokenContainer
     * @param int $type
     * @throws Exception
     */
    protected function assertAndSkip(TokenContainer $tokenContainer, int $type)
    {
        $this->assertType($tokenContainer->current(), $type);
        $tokenContainer->next();
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeBlock
     * @throws Exception
     */
    protected function parseBlock(ScopeRef $scopeRef, TokenContainer $tokenContainer): NodeBlock
    {
        $block = new NodeBlock();
        if (!$tokenContainer->current()->isType(Token::T_BRACKETS_C_OP)) {
            $block->add($this->parseBlockStatement($scopeRef, $tokenContainer));

            return $block;
        }

        $tokenContainer->next();
        while (!$tokenContainer->current()->isType(Token::T_BRACKETS_C_CL)) {
            $block->add($this->parseBlockStatement($scopeRef, $tokenContainer));
        }
        $tokenContainer->next();

        return $block;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return INode
     * @throws Exception
     */
    protected function parseBlockStatement(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        $token = $tokenContainer->current();
        foreach ($this->parsersBag as $parser) {
            if ($parser->can($token)) {
                return $parser->parse($scopeRef, $tokenContainer);
            }
        }

        throw new UndefinedToken($token);
    }


}