<?php

namespace Expression\Parser;


use Exception;
use Expression\Node\INode;
use Expression\Node\NodeBlock;
use Expression\Node\NodeReturn;
use Expression\Node\NodeValue;
use Expression\Node\NodeVarLetDeclarator;
use Expression\Parser\Exception\UndefinedToken;
use Expression\Scope\ScopeRef;
use Expression\Token;
use Expression\TokenContainer;

class SyntaxParser extends ParserAbstract
{

    protected const ACCEPT_TOKEN = [
        Token::T_RETURN,
        Token::T_VAR_DECLARATOR
    ];

    public function parse(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        $token = $tokenContainer->current();

        switch ($token->getType()) {
            case Token::T_RETURN:
                return $this->parseReturn($scopeRef, $tokenContainer);
            case Token::T_VAR_DECLARATOR:
                return $this->parseInitVariable($scopeRef, $tokenContainer);
        }

        throw new UndefinedToken($token);
    }


    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return INode
     * @throws \Exception
     */
    protected function parseInitVariable(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        $tokenContainer->next();
        $nodeBlock = new NodeBlock();
        $nodeBlock->add(new NodeVarLetDeclarator(new NodeValue($tokenContainer->current()->getValue()), $scopeRef));
        $nodeBlock->add($this->parsersBag->get(ExpressionParser::class)->parse($scopeRef, $tokenContainer));

        return $nodeBlock;
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeReturn
     * @throws Exception
     */
    protected function parseReturn(ScopeRef $scopeRef, TokenContainer $tokenContainer)
    {
        $tokenContainer->next();
        return new NodeReturn($this->parsersBag->get(ExpressionParser::class)->parse($scopeRef, $tokenContainer));
    }

}