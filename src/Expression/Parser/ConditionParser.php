<?php

namespace Expression\Parser;


use Exception;
use Expression\Node\INode;
use Expression\Node\NodeBlock;
use Expression\Node\NodeCondition;
use Expression\Node\NodeValue;
use Expression\Parser\Exception\UndefinedToken;
use Expression\Scope\ScopeRef;
use Expression\Token;
use Expression\TokenContainer;

class ConditionParser extends ParserAbstract
{

    protected const ACCEPT_TOKEN = [
        Token::T_IF
    ];

    public function parse(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        $token = $tokenContainer->current();
        switch ($token->getType()){
            case Token::T_IF:
                $tokenContainer->next();
                return $this->getConditionNode($scopeRef, $tokenContainer);
        }

        throw new UndefinedToken($token);
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeCondition
     * @throws Exception
     */
    protected function getConditionNode(ScopeRef $scopeRef, TokenContainer $tokenContainer): NodeCondition
    {
        return new NodeCondition(
            $this->parsersBag->get(ExpressionParser::class)->parse($scopeRef, $tokenContainer),
            $this->parseBlock($scopeRef, $tokenContainer),
            $this->parseChain($scopeRef, $tokenContainer)
        );
    }

    /**
     * @param ScopeRef $scopeRef
     * @param TokenContainer $tokenContainer
     * @return NodeCondition|null
     * @throws Exception
     */
    protected function parseChain(ScopeRef $scopeRef, TokenContainer $tokenContainer): ?NodeCondition
    {
        switch ($tokenContainer->current()->getType()) {
            case Token::T_ELSE_IF:
                $tokenContainer->next();
                return $this->getConditionNode($scopeRef, $tokenContainer);
            case Token::T_ELSE:
                $tokenContainer->next();
                return new NodeCondition(new NodeValue(TRUE), $this->parseBlock($scopeRef, $tokenContainer));
            default:
                return null;
        }
    }

}