<?php

namespace Expression;


use Expression\Node\INode;
use Expression\Node\NodeApp;
use Expression\Node\NodeBlock;
use Expression\Parser\Exception\UndefinedToken;
use Expression\Parser\ParserAbstract;
use Expression\Scope\ScopeRef;

class AppParser extends ParserAbstract
{

    public function parse(ScopeRef $scopeRef, TokenContainer $tokenContainer): INode
    {
        $expression = new NodeBlock();

        while (true) {
            $token = $tokenContainer->current();
            foreach ($this->parsersBag as $parser) {
                if (!$parser->can($token)) {
                    continue;
                }

                $expression->add($parser->parse($scopeRef, $tokenContainer));
                continue 2;
            }

            if ($token->isType(Token::EOF)) {
                break;
            }

            throw new UndefinedToken($token);
        }

        return new NodeApp($scopeRef->getScope(), $expression);
    }

}