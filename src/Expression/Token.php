<?php


namespace Expression;

/**
 * Class Token
 * @package Expression
 * LAST_TOKEN_NUMBER = 29
 */
class Token
{

    const T_VAR_DECLARATOR = 1;

    const T_IF = 4;
    const T_ELSE_IF = 5;
    const T_ELSE = 6;

    const T_RETURN = 7;

    const T_BRACKETS_R_OP = 8;
    const T_BRACKETS_R_CL = 9;

    const T_BRACKETS_C_OP = 10;
    const T_BRACKETS_C_CL = 11;

    const T_BRACKETS_S_OP = 12;
    const T_BRACKETS_S_CL = 13;

    const T_COMMA = 14;
    const T_SEMICOLON = 15;
    const T_DOT = 16;
    const T_NUMBER = 17;
    const T_TEXT = 18;
    const T_VAR = 19;

    const T_FUNC = 23;
    const T_FUNC_ARROW = 29;

    const T_LOOP_FOR = 24;
    const T_LOOP_CONTINUE = 25;
    const T_LOOP_BREAK = 26;

    const T_OPERATOR_BINARY = 27;
    const T_OPERATOR_UNARY = 28;

    const EOF = 22;

    /** @var int  */
    protected $type;

    /** @var mixed */
    protected $value;

    /** @var int  */
    protected $start;

    /** @var int  */
    protected $end;

    public function __construct(int $type, $value, int $startCursor, int $endCursor)
    {
        $this->type = $type;
        $this->value = $value;
        $this->start = $startCursor;
        $this->end = $endCursor;
    }

    public function isType(int $type): bool
    {
        return $this->type === $type;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getEndCursor(): int
    {
        return $this->end;
    }


}