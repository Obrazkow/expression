<?php

namespace Expression\Node;

class NodeNumber implements INode
{

    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function compute()
    {
        return +$this->value;
    }

}