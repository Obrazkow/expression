<?php

namespace Expression\Node;


interface INodeVariable extends INode
{

    public function assign($value);

}