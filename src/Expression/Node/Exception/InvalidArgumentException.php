<?php


namespace Expression\Node\Exception;



use Exception;

class InvalidArgumentException extends Exception
{

    public function __construct(int $expected, int $actual)
    {
        parent::__construct(sprintf('Too few arguments %s passed %s expected', $actual, $expected));
    }

}