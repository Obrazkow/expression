<?php

namespace Expression\Node;


class NodeBlock implements INode
{

    /** @var INode[] */
    protected $nodes = [];

    public function add(INode $node)
    {
        $this->nodes[] = $node;
    }

    public function compute()
    {
        foreach ($this->nodes as $node) {
            if ($node instanceof NodeReturn) {
                return $node;
            }

            $result = $node->compute();
            if ($result instanceof NodeReturn) {
                return $result;
            }
        }

        return null;
    }

}