<?php

namespace Expression\Node;

use Expression\Structure\HashMapStructure;

class NodeHashMapStructure implements INode
{

    public function compute()
    {
        return new HashMapStructure();
    }

}