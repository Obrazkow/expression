<?php

namespace Expression\Node;

class NodeString implements INode
{

    protected $value;

    public function __construct($value)
    {
        $this->value = (string) $value;
    }

    public function compute(): string
    {
        return $this->value;
    }

}