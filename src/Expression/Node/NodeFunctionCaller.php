<?php

namespace Expression\Node;


class NodeFunctionCaller implements INode
{

    protected $variable;
    protected $arguments;

    public function __construct(INode $variable, NodeArguments $arguments)
    {
        $this->variable = $variable;
        $this->arguments = $arguments;
    }

    public function compute()
    {
        return call_user_func_array($this->variable->compute(), $this->arguments->compute());
    }

}