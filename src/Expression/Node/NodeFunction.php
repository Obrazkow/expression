<?php

namespace Expression\Node;

use Exception;
use Expression\Scope\Exception\VariableAlreadyDefined;
use Expression\Scope\ScopeRef;

class NodeFunction implements INode
{

    protected $variables = [];
    protected $body;
    protected $name;
    protected $scope;

    public function __construct(NodeValue $functionName, NodeVarList $variables, INode $body, ScopeRef $scope)
    {
        $this->name = $functionName->compute();
        $this->variables = $variables;
        $this->body = $body;
        $this->scope = $scope;
    }

    /**
     * @param $args
     * @return mixed|null
     * @throws Exception
     */
    public function __invoke(...$args)
    {
        return $this->compute(...$args);
    }

    /**
     * @param array $args
     * @return mixed|null
     * @throws VariableAlreadyDefined|Exception
     */
    public function compute(...$args)
    {
        $this->variables->compute(...$args);

        if ($this->body instanceof NodeBlock) {
            $result = $this->body->compute();
            $result = $result instanceof NodeReturn ? $result->compute() : null;
        } else {
            $result = $this->body->compute();
            $result = $this->body instanceof NodeReturn ? $result : null;
        }

        $this->scope->reset();

        return $result;
    }

}