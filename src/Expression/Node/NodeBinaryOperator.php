<?php

namespace Expression\Node;

use Expression\Operator\IBinaryOperator;

class NodeBinaryOperator implements INode
{

    protected $operator;
    protected $argFirst;
    protected $argSecond;

    public function __construct(IBinaryOperator $operator, INode $argFirst, INode $argSecond)
    {
        $this->operator = $operator;
        $this->argFirst = $argFirst;
        $this->argSecond = $argSecond;
    }

    public function compute()
    {
        return $this->operator->compute($this->argFirst, $this->argSecond);
    }

}