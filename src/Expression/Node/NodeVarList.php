<?php

namespace Expression\Node;


use Exception;
use Expression\Node\Exception\InvalidArgumentException;
use Expression\Scope\Exception\VariableAlreadyDefined;
use Expression\Scope\ScopeRef;

class NodeVarList implements INode
{

    protected $scopeRef;

    /** @var NodeVar[] */
    protected $variables = [];

    public function __construct(ScopeRef $scopeRef)
    {
        $this->scopeRef = $scopeRef;
    }

    public function addArgument(NodeVar $variable)
    {
        $this->variables[] = $variable;
    }

    /**
     * @inheritDoc
     * @throws VariableAlreadyDefined
     * @throws Exception
     */
    public function compute(...$args)
    {
        if (count($args) < count($this->variables)) {
            throw new InvalidArgumentException(count($this->variables), count($args));
        }

        foreach ($this->variables as $index => $variable) {
            $this->scopeRef->initVariable($variable->getName(), $args[$index]);
        }
    }

}