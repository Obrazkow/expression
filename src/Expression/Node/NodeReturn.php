<?php

namespace Expression\Node;

class NodeReturn implements INode
{

    protected $value;

    public function __construct(INode $value)
    {
        $this->value = $value;
    }

    public function compute()
    {
        return $this->value->compute();
    }

}