<?php

namespace Expression\Node;


class NodeArguments implements INode
{

    /** @var INode[] */
    protected $arguments = [];

    public function addArgument(INode $argument)
    {
        $this->arguments[] = $argument;
    }

    public function compute(): array
    {
        $arguments = [];

        foreach ($this->arguments as $argument) {
            $arguments[] = $argument->compute();
        }

        return $arguments;
    }

}