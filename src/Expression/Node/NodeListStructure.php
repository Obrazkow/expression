<?php

namespace Expression\Node;

use Expression\Structure\ListStructure;

class NodeListStructure implements INode
{

    public function compute()
    {
        return new ListStructure();
    }

}