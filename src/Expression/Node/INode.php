<?php

namespace Expression\Node;


interface INode
{

    public function compute();

}