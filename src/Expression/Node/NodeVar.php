<?php

namespace Expression\Node;

use Exception;
use Expression\Scope\Exception\UndefinedVariable;
use Expression\Scope\ScopeRef;

class NodeVar implements INodeVariable
{

    protected $variableName;
    protected $scopeRef;

    public function __construct(string $variableName, ScopeRef $scopeRef)
    {
        $this->variableName = $variableName;
        $this->scopeRef = $scopeRef;
    }

    /**
     * @inheritDoc
     * @throws UndefinedVariable
     */
    public function compute()
    {
        return $this->scopeRef->resolveVariable($this->variableName);
    }

    /**
     * @param $value
     * @throws Exception
     */
    public function assign($value)
    {
        $this->scopeRef->setVariable($this->variableName, $value);
    }

    public function getName()
    {
        return $this->variableName;
    }

}