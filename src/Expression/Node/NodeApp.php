<?php


namespace Expression\Node;


use Expression\Scope\Scope;

class NodeApp implements INode
{

    protected $scope;
    protected $body;

    public function __construct(Scope $scope, NodeBlock $body)
    {
        $this->scope = $scope;
        $this->body = $body;
    }

    public function compute()
    {
        $result = $this->body->compute();
        $result = $result instanceof NodeReturn ? $result->compute() : null;
        $this->scope->reset();

        return $result;
    }
}