<?php

namespace Expression\Node;


use Exception;
use Expression\Structure\StructureInterface;

class NodeObject implements INodeVariable
{

    protected $nodeVariable;
    protected $property;

    public function __construct(INode $nodeVariable, INode $property)
    {
        $this->nodeVariable = $nodeVariable;
        $this->property = $property;
    }

    /**
     * @throws Exception
     */
    public function compute()
    {
        $object = $this->nodeVariable->compute();
        $propertyName = $this->property->compute();

        $this->validateObject($object);
        $this->validatePropertyName($propertyName);

        if (is_array($object)) {
            return $object[$propertyName] ?? null;
        }

        if (is_callable([$object, $propertyName])) {
            return [$object, $propertyName];
        }

        return $object->{$propertyName};
    }

    /**
     * @param $value
     * @throws Exception
     */
    public function assign($value)
    {
        $object = $this->nodeVariable->compute();
        $propertyName = $this->property->compute();

        $this->validateObject($object);
        $this->validatePropertyName($propertyName);

        if (is_array($object)) {
            $object[$propertyName] = $value;;
            return;
        }

        $object->{$propertyName} = $value;
    }

    /**
     * @param $object
     * @throws Exception
     */
    protected function validateObject($object)
    {
        if (is_callable($object)) {
            throw new Exception('Expected StructureInterface or array type, but got closure');
        }

        if (!$object instanceof StructureInterface && !is_array($object)) {
            throw new Exception(sprintf('Expected StructureInterface or array type, but got %s', gettype($object)));
        }
    }

    /**
     * @param $propertyName
     * @throws Exception
     */
    protected function validatePropertyName($propertyName)
    {
        if (!is_string($propertyName) && !is_int($propertyName)) {
            throw new Exception(sprintf(' Illegal offset type %s', gettype($propertyName)));
        }
    }

}