<?php

namespace Expression\Node;

use Expression\Scope\Exception\VariableAlreadyDefined;
use Expression\Scope\ScopeRef;

class NodeVarLetDeclarator implements INode
{

    protected $variableName;
    protected $scopeRef;

    public function __construct(NodeValue $variableName, ScopeRef $scopeRef)
    {
        $this->variableName = $variableName->compute();
        $this->scopeRef = $scopeRef;
    }

    /**
     * @inheritDoc
     * @throws VariableAlreadyDefined
     */
    public function compute()
    {
        $this->scopeRef->initVariable($this->variableName);
    }

}