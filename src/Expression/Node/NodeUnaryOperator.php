<?php

namespace Expression\Node;

use Expression\Operator\IUnaryOperator;

class NodeUnaryOperator implements INode
{

    protected $operator;
    protected $arg;

    public function __construct(IUnaryOperator $operator, INode $arg)
    {
        $this->operator = $operator;
        $this->arg = $arg;
    }

    public function compute()
    {
        return $this->operator->compute($this->arg);
    }

}