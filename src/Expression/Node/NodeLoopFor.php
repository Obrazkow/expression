<?php

namespace Expression\Node;


use Expression\Scope\ScopeRef;
use Expression\Token;

class NodeLoopFor implements INode
{

    protected $initialAction;
    protected $condition;
    protected $iterationAction;
    protected $bodyScope;
    protected $body;

    public function __construct(
        INode $initialAction,
        INode $condition,
        INode $iterationAction,
        ScopeRef $bodyScope,
        NodeBlock $body
    ) {
        $this->initialAction = $initialAction;
        $this->condition = $condition;
        $this->iterationAction = $iterationAction;
        $this->bodyScope = $bodyScope;
        $this->body = $body;
    }

    public function compute()
    {
        for ($this->initialAction->compute(); $this->condition->compute(); $this->iterationAction->compute()) {
            $this->bodyScope->reset();
            $statement = $this->body->compute();

            if ($statement instanceof NodeLoopInterruption) {
                if ($statement->compute() === Token::T_LOOP_BREAK) {
                    break;
                }

                continue;
            }

            if ($statement instanceof NodeReturn) {
                return $statement;
            }
        }

        return null;
    }

}