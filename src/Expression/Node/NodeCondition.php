<?php

namespace Expression\Node;



class NodeCondition implements INode
{

    protected $body;
    protected $chain;
    protected $condition;

    public function __construct(INode $condition, NodeBlock $body, NodeCondition $chain = null)
    {
        $this->condition = $condition;
        $this->body = $body;
        $this->chain = $chain;
    }

    public function compute()
    {
        if ($this->condition->compute()) {
            return $this->body->compute();
        }

        if ($this->chain) {
            return $this->chain->compute();
        }

        return null;
    }

}