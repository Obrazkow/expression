<?php

namespace Expression\Scope;


use Expression\Scope\Exception\UndefinedVariable;
use Expression\Scope\Exception\VariableAlreadyDefined;

class ScopeRef
{

    private $id;
    private $scope;
    private $scopeChain;

    final public function __construct(int $id, Scope $scope, ScopeRef $scopeRefChain = null)
    {
        $this->id = $id;
        $this->scope = $scope;
        $this->scopeChain = $scopeRefChain;
    }

    /**
     * @param string $name
     * @param $value
     * @throws VariableAlreadyDefined
     */
    public function setVariable(string $name, $value)
    {
        if (!$this->scopeChain || $this->scope->isInitializedVariable($this->id, $name)) {
            $this->scope->setVariable($this->id, $name, $value);
        } else {
            $this->scopeChain->setVariable($name, $value);
        }
    }

    /**
     * @param string $name
     * @return mixed
     * @throws UndefinedVariable
     */
    public function resolveVariable(string $name)
    {
        if (!$this->scopeChain || $this->scope->isInitializedVariable($this->id, $name)) {
            return $this->scope->resolveVariable($this->id, $name);
        }

        return $this->scopeChain->resolveVariable($name);
    }

    /**
     * @param string $name
     * @param null $value
     * @throws VariableAlreadyDefined
     */
    public function initVariable(string $name, $value = null)
    {
        $this->scope->initVariable($this->id, $name, $value);
    }

    public function getScope(): Scope
    {
        return $this->scope;
    }

    public function reset()
    {
        $this->scope->reset($this->id);
    }

}