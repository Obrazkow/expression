<?php

namespace Expression\Scope;


use Exception;
use Expression\Scope\Exception\VariableAlreadyDefined;
use Expression\Scope\Exception\UndefinedVariable;

class Scope
{

    protected $variables = [];

    protected $initiatedConstants = [];
    protected $constants = [];
    protected $refsCounter = 0;

    public static function default(): Scope
    {
        $scope = new static();
        $scope->setConstants('null', null);
        $scope->setConstants('false', false);
        $scope->setConstants('true', true);

        return $scope;
    }

    public function generateRef(): ScopeRef
    {
        $ref = new ScopeRef($this->refsCounter, $this);
        $this->refsCounter++;

        return $ref;
    }

    public function generateRefChain(ScopeRef $scopeRef)
    {
        $ref = new ScopeRef($this->refsCounter, $this, $scopeRef);
        $this->refsCounter++;

        return $ref;
    }

    /**
     * @param int $scopeRefId
     * @param string $name
     * @param $value
     * @throws VariableAlreadyDefined
     * @throws Exception
     */
    public function setVariable(int $scopeRefId, string $name, $value)
    {
        if (isset($this->initiatedConstants[$name])) {
            throw new VariableAlreadyDefined("Cannot override variable with name ". $name);
        }

        if (!isset($this->variables[$scopeRefId][$name]['init'])) {
            throw new \Exception(sprintf("Variable %s is not initialized", $name));
        }

        if (!isset($this->variables[$scopeRefId])) {
            $this->variables[$scopeRefId] = [];
        }

        $this->variables[$scopeRefId][$name]['value'] = $value;
    }

    /**
     * Constant visible in all scope of expression.
     * @param string $name
     * @param $value
     */
    public function setConstants(string $name, $value)
    {
        $this->constants[$name] = $value;
        $this->initiatedConstants[$name] = true;
    }

    /**
     * @param int $scopeRefId
     * @param string $name
     * @return mixed
     * @throws UndefinedVariable
     */
    public function resolveVariable(int $scopeRefId, string $name)
    {
        $lowerName = strtolower($name);
        if (isset($this->initiatedConstants[$lowerName])) {
            return $this->constants[$lowerName];
        }

        if (isset($this->variables[$scopeRefId][$name]['init'])) {
            return $this->variables[$scopeRefId][$name]['value'];
        }

        throw new UndefinedVariable(sprintf('Undefined variable %s', $name));
    }

    /**
     * @param int $scopeRefId
     * @param string $name
     * @param null $value
     * @throws VariableAlreadyDefined
     */
    public function initVariable(int $scopeRefId , string $name, $value = null)
    {
        if (isset($this->initiatedConstants[$name])) {
            throw new VariableAlreadyDefined("Cannot override variable with name ". $name);
        }

        if (isset($this->variables[$scopeRefId][$name]['init'])) {
            throw new VariableAlreadyDefined(sprintf("Variable %s already defined", $name));
        }

        if (!isset($this->variables[$scopeRefId])) {
            $this->variables[$scopeRefId] = [];
        }

        $this->variables[$scopeRefId][$name]['init'] = true;
        if ($value !== null) {
            $this->variables[$scopeRefId][$name]['value'] = $value;
        }
    }

    public function reset(int $scopeRefId = null)
    {
        if ($scopeRefId) {
            $this->variables[$scopeRefId] = [];
        } else {
            $this->variables = [];
        }
    }

    public function isInitializedVariable(int $scopeRefId, string $name)
    {
        return isset($this->variables[$scopeRefId][$name]['init']);
    }

}