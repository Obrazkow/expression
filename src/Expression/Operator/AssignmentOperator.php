<?php

namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\INodeVariable;
use Expression\Operator\Exception\InvalidTypeException;

class AssignmentOperator extends Operator implements IBinaryOperator
{

    const SIGN = '=';

    /**
     * @param $a
     * @param $b
     * @return float|int
     * @throws InvalidTypeException
     */
    public function compute(INode $a, INode $b)
    {
        if (!$a instanceof INodeVariable) {
            throw new InvalidTypeException(INodeVariable::class);
        }

        return $a->assign($b->compute());
    }

}