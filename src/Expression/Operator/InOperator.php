<?php

namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Structure\IterableStructureAbstract;
use Expression\Structure\ListStructure;

class InOperator extends Operator implements IBinaryOperator
{

    const SIGN = 'in';

    public function compute(INode $a, INode $b): bool
    {
        $var = $b->compute();

        if (is_string($var)) {
            return strpos($var, $a->compute()) !== false;
        }

        if ($var instanceof ListStructure) {
            return $var->exists($a->compute());
        }

        return false;
    }

}