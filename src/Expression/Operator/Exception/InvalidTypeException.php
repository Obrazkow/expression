<?php


namespace Expression\Operator\Exception;


use Exception;

class InvalidTypeException extends Exception
{

    public function __construct(string $type)
    {
        parent::__construct('Variable must be instanceof ' . $type);
    }

}