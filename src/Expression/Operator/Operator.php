<?php

namespace Expression\Operator;


abstract class Operator
{

    const LEFT_ASSOC = true;

    public function isLeftAssoc(): bool
    {
        return static::LEFT_ASSOC;
    }

}