<?php


namespace Expression\Operator;


use Expression\Node\INode;

class EqualOperator extends Operator implements IBinaryOperator
{

    const SIGN = '==';

    public function compute(INode $a, INode $b): bool
    {
        return $a->compute() == $b->compute();
    }

}