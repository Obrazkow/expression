<?php

namespace Expression\Operator;


use Expression\Node\INode;

class AndOperator extends Operator implements IBinaryOperator
{

    const SIGN = '&&';

    public function compute(INode $a, INode $b): bool
    {
        return $a->compute() && $b->compute();
    }

}