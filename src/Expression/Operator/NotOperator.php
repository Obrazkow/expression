<?php

namespace Expression\Operator;


use Expression\Node\INode;

class NotOperator extends Operator implements IUnaryOperator
{

    const SIGN = '!';

    public function compute(INode $a): bool
    {
        return !$a->compute();
    }

}