<?php

namespace Expression\Operator;


use Expression\Node\INode;
use Expression\Node\INodeVariable;
use Expression\Operator\Exception\InvalidTypeException;

class PostDecrementOperator extends Operator implements IUnaryOperator
{

    const SIGN = '--';

    const LEFT_ASSOC = false;

    /**
     * @param INode $a
     * @return int|float
     * @throws InvalidTypeException
     */
    public function compute(INode $a)
    {
        if (!$a instanceof INodeVariable) {
            throw new InvalidTypeException(INodeVariable::class);
        }

        $result = $a->compute();
        $a->assign($result - 1);

        return $result;
    }

}