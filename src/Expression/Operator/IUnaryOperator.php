<?php

namespace Expression\Operator;


use Expression\Node\INode;

interface IUnaryOperator extends IOperator
{

    public function compute(INode $a);

}