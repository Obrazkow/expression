<?php

namespace Expression\Operator;


use Expression\Node\INode;

interface IBinaryOperator extends IOperator
{

    public function compute(INode $a, INode $b);

}