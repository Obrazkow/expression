<?php

namespace Expression\Operator;


use Expression\Node\INode;

class MultiplicationOperator extends Operator implements IBinaryOperator
{

    const SIGN = '*';

    public function compute(INode $a, INode $b)
    {
        return $a->compute() * $b->compute();
    }

}