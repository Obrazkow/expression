<?php


namespace Expression\Operator;


interface OperatorPrecedence
{

    public const PRECEDENCE = [
        InOperator::class => 500,

        PostDecrementOperator::class => 21,
        PostIncrementOperator::class => 21,
        PreDecrementOperator::class => 21,
        PreIncrementOperator::class => 21,

        NotOperator::class => 19,

        DivisionOperator::class => 18,
        MultiplicationOperator::class => 18,

        PlusOperator::class => 17,
        MinusOperator::class => 17,

        GTEOperator::class => 15,
        GTOperator::class => 15,
        LTEOperator::class => 15,
        LTOperator::class => 15,

        NotIdenticalOperator::class => 14,
        IdenticalOperator::class => 14,
        NotEqualOperator::class => 14,
        EqualOperator::class => 14,
        SpaceshipOperator::class => 14,

        AndOperator::class => 10,

        OrOperator::class => 9,

        AssignmentOperator::class => 6,
    ];

}