<?php

namespace Expression\Operator;


use Expression\Node\INode;

class PlusOperator extends Operator implements IBinaryOperator
{

    const SIGN = '+';

    public function compute(INode $a, INode $b)
    {
        $a = $a->compute();
        $b = $b->compute();

        if (is_string($a) || is_string($b)) {
            return $a . $b;
        }

        return $a + $b;
    }

}