<?php

namespace Expression\Operator;



interface IOperator
{

    public function isLeftAssoc(): bool;

}