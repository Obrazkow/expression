<?php

namespace Expression\Operator;


use Expression\Node\INode;

class OrOperator extends Operator implements IBinaryOperator
{

    const SIGN = '||';

    public function compute(INode $a, INode $b): bool
    {
        return (bool) $a->compute() || (bool) $b->compute();
    }

}