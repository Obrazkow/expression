<?php


namespace Expression;


use Generator;

class TokenContainer implements \Iterator
{

    private $tokens = [];
    private $cursor = 0;

    public function add(Token $token)
    {
        $this->tokens[] = $token;
    }

    /**
     * @param int $skip
     * @return Generator|Token[]
     */
    public function lookAhead(int $skip = 0): \Generator
    {
        $count = count($this->tokens);

        for ($i = $this->cursor + $skip; $i < $count; $i++) {
            yield $this->tokens[$i];
        }
    }

    public function shift(): Token
    {
        $token = $this->current();
        $this->next();

        return $token;
    }

    public function next()
    {
        $this->cursor++;
    }

    public function key()
    {
        return $this->cursor;
    }

    public function valid()
    {
        return isset($this->tokens[$this->cursor]);
    }

    public function rewind()
    {
        $this->cursor = 0;
    }

    public function current(): Token
    {
        return $this->tokens[$this->cursor];
    }

}